<?php

session_start();
if(isset($_SESSION['username']) && (!isset($_SESSION['key']))){
   header('location:account.php?q=1');
}
else if(isset($_SESSION['username']) && isset($_SESSION['key']) && $_SESSION['key'] == '54585c506829293a2d4c3b68543b316e2e7a2d277858545a36362e5f39'){
   header('location:dash.php?q=0');
}
else{}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" href="image/logo.png" type="image/icon" sizes="18x18">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title> LNU-CTE Reviewer </title>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
<link  rel="stylesheet" href="css/footer.css">
 <link rel="stylesheet" href="css/style.css">
 <link  rel="stylesheet" href="css/font.css">
 <link rel="stylesheet" href="css/dashboard.css">
 <script src="js/bootstrap.min.js"  type="text/javascript"></script>
  <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <script src="js/script.js" defer></script>
</head>
<body>

<!--navbar start-->

<nav class="navbar navbar-expand-lg navbar-light  sticky-top" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="#"><img src="image/logo.png" alt="Logo" style="width:88px;">&nbsp  CTE- Reviewer Center </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 
  <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
    <ul class="navbar-nav ">
      <li class="nav-item ">
      <a class="nav-link" href="index.php"><i class="fas fa-home"></i>&nbsp Home</a>
      </li>
      &nbsp
      <li class="nav-item">
        <a class="nav-link" href="enrollment.php"><i class="fas fa-calendar-alt"></i>&nbsp Enrollment</a>
      </li>
      &nbsp
      <li class="nav-item">
      <a class="nav-link" href="enrollment_form.php"><i class="fas fa-question-circle"></i>&nbsp About</a>
      </li>
    </ul>
  </div>
</nav>

<!--navbar end-->
 
 <br>
 <!--enrollment starts-->
 <form action="sign.php?q=account.php" method="POST" class="form">
 <fieldset>
      <h1 class="text-center"><i class="fas fa-calendar-alt"></i>&nbspEnrollment Form</h1>
      <!-- Progress bar -->
      <div class="progressbar">
        <div class="progress" id="progress"></div>
        
        <div
          class="progress-step progress-step-active"
          data-title="Personal "
        ></div>
        <div class="progress-step" data-title="Contact"></div>
        <div class="progress-step" data-title="ID"></div>
        <div class="progress-step" data-title="School"></div>
        <div class="progress-step" data-title="Payment"></div>
        <div class="progress-step" data-title="Password"></div>
      </div>

      <!-- Steps -->
      <div class="form-step form-step-active">

      
        <div class="form-group">
          <label for="name">First Name</label>
          <input type="text"  class="form-control" name="name" id="name"  value="<?php
if (isset($_GET['name']))
{
echo $_GET['name'];
}?>"/>
        </div>
        <div class="form-group">
          <label for="lname">Last Name</label>
          <input type="text"  class="form-control" name="lname" id="lname"  value="<?php
if (isset($_GET['lname']))
{
echo $_GET['lname'];
}?>"/>
        </div>
        <div class="form-group">
          <label for="mname">Middle Name</label>
          <input type="text"  class="form-control" name="mname" id="mname"  value="<?php
if (isset($_GET['mname']))
{
echo $_GET['mname'];
}?>" />
        </div>
        <div class="form-group">
    <label for="gender">Gender</label>
    <select class="form-control"  name="gender" id="gender">
    <option value="" selected>Choose...</option>
      <option value="M"<?php
  if (isset($_GET['gender']))
  {
if ($_GET['gender'] == "M")
    echo "selected";
  }
?>>Male</option>
      <option value="F"<?php
  if (isset($_GET['gender']))
  {
if ($_GET['gender'] == "F")
    echo "selected";
  }
?>>Female</option>
      
    </select>
  </div>
        <div class="">
          <a href="#" class="btn btn-next width-50 ml-auto">Next</a>
        </div>
      </div>
      <div class="form-step">
        <div class="form-group">
          <label for="phone">Contact Number (Globe or TM Only)</label>
          <input type="text" class="form-control" name="phno" id="phno" value="<?php
if (isset($_GET['phno']))
{
echo $_GET['phno'];
}
?>"/>
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="text"  class="form-control" name="email" id="email" value="<?php
if (isset($_GET['email']))
{
echo $_GET['email'];
}
?>" />
        </div>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      <div class="form-step">
        <div class="form-group">
          <label for="student_id">Student ID</label>
          <input type="number"  class="form-control" name="student_id" id="student_id" value="<?php
if (isset($_GET['student_id']))
{
echo $_GET['student_id'];
}
?>"/>
        </div>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      <div class="form-step">
    <div class="form-group">
    <label for="degree">Degree</label>
    <select class="form-control" name="degree" id="id">
    <option  value="" selected>Choose...</option>
      <option value="BEEd"<?php
  if (isset($_GET['degree']))
  {
if ($_GET['degree'] == "BEEd")
    echo "selected";
  }
?>>BEEd</option>
      <option value="BSed"<?php
  if (isset($_GET['degree']))
  {
if ($_GET['degree'] == "BSed")
    echo "selected";
  }
?>>BSed</option>
      <option value="BEEd (Supplemental)"<?php
  if (isset($_GET['degree']))
  {
if ($_GET['degree'] == "BEEd (Supplemental)")
    echo "selected";
  }
?>>BEEd (Supplemental)</option>
	  <option value="BSed (TCP)"<?php
  if (isset($_GET['degree']))
  {
if ($_GET['degree'] == "BSed (TCP)")
    echo "selected";
  }
?>>BSed (TCP)</option>
    </select>
  </div>
  <div class="form-group">
    <label for="school">School Graduate</label>
    <input type="text" class="form-control" id="school_grad" name="school_grad" value="<?php
if (isset($_GET['school_grad']))
{
echo $_GET['school_grad'];
}
?>">
  </div>
  <div class="form-group">
    <label for="year">Year Graduate</label>
    <input type="text" class="form-control" id="year_grad" name="year_grad" value="<?php
if (isset($_GET['year_grad']))
{
echo $_GET['year_grad'];
}
?>">
  </div>
  <div class="form-group">
    <label for="award">Academic Awards</label>
    <select class="form-control" name="award" id="award">
    <option  value="" selected>Choose...</option>
      <option value="Cumlaude"<?php
  if (isset($_GET['award']))
  {
if ($_GET['award'] == "Cumlaude")
    echo "selected";
  }
?>>Cumlaude</option>
      <option value="Magna Cumlaude"<?php
  if (isset($_GET['award']))
  {
if ($_GET['award'] == "Magna Cumlaude")
    echo "selected";
  }
?>>Magna Cumlaude</option>
      <option value="Summa Cumlaude"<?php
  if (isset($_GET['award']))
  {
if ($_GET['award'] == "Summa Cumlaude")
    echo "selected";
  }
?>>Summa Cumlaude</option>
	  <option value="None"<?php
  if (isset($_GET['award']))
  {
if ($_GET['award'] == "None")
    echo "selected";
  }
?>>None</option>
    </select>
  </div>
  <div class="form-group">
    <label for="take">First Taker</label>
    <select class="form-control" name="take" id="take">
    <option   value="" selected>Choose...</option>
      <option  value="Yes"<?php
  if (isset($_GET['take']))
  {
if ($_GET['take'] == "Yes")
    echo "selected";
  }
?>>Yes</option>
      <option  value="No"<?php
  if (isset($_GET['take']))
  {
if ($_GET['take'] == "No")
    echo "selected";
  }
?>>No</option>
    
    </select>
  </div>
  <div class="form-group">
    <label for="majorship">Majorship</label>
    <select class="form-control" name="majorship" id="majorship">
    <option   value="" selected>Choose...</option>
      <option value="English"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "English")
    echo "selected";
  }
?>>English</option>
      <option value="Filipino"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "Filipino")
    echo "selected";
  }
?>>Filipino</option>
      <option value="Mathemathics"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "Mathemathics")
    echo "selected";
  }
?>>Mathemathics</option>
	  <option value="Social Science"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "Social Science")
    echo "selected";
  }
?>>Social Science</option>
	  <option value="Biological Science"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "Biological Science")
    echo "selected";
  }
?>>Biological Science</option>
	  <option value="Physical Science"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "Physical Science")
    echo "selected";
  }
?>>Physical Science</option>
	  <option value="MAPEH"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "MAPEH")
    echo "selected";
  }
?>>MAPEH</option>
	  <option value="TLE"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "TLE")
    echo "selected";
  }
?>>TLE</option>
	  <option value="Fishery Arts"<?php
  if (isset($_GET['majorship']))
  {
if ($_GET['majorship'] == "Fishery Arts")
    echo "selected";
  }
?>>Fishery Arts</option>
    </select>
  </div>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      <div class="form-step">
      <label><b> Mode of Payment:</b><p> 1. Personal Transaction at LNU Cashier's Office - Present QR Code at LNU Gate
    (Note: Go first to LNU CTE Office for the Registration Form to be presented at LNU Cashier)</p>

<p>2. Palawan Express Padala with details below:
          Name: Ruby Mae H. Peñaranda
          Mobile Num: 0967-253-0502
          Purpose of Transaction: LET Review Registration Fee</p>

<p>3. Gcash 
     (Note: Add 2% of the Total Amount of Payment for the Cash Out Service Fee)
     Send To: 09672530502
     Amount: Payment  + 2% Service Fee = Total Amount
     Message: 
        Name of Reviewee: _________________</p>

<p>Send picture of payment receipt to ctereview2021@gmail.com for confirmation.</p></label>

<div class="col-md-4 mb-3">
    <label for="validationCustom05">Payment Method</label>
    <select class="form-control" name="pay" id="validationCustom05">
	<option  value="" selected>Choose...</option>
      <option value="Personal Transaction at LNU Cashier's Office"<?php
  if (isset($_GET['pay']))
  {
if ($_GET['pay'] == "Personal Transaction at LNU Cashier's Office")
    echo "selected";
  }
?>> Personal Transaction at LNU Cashier's Office</option>
      <option value="Palawan"<?php
  if (isset($_GET['pay']))
  {
if ($_GET['pay'] == "Palawan")
    echo "selected";
  }
?>>Palawan</option>
	  <option value="Gcash" <?php
  if (isset($_GET['pay']))
  {
if ($_GET['pay'] == "Gcash")
    echo "selected";
  }
?>>Gcash</option>
     
      
    </select>
	</div>
	<div class="form-group">
    <label for="exampleFormControlFile1">Picture of Signature Electronic signature (E-sign)</label>
    <input type="file" class="form-control-file" name="image" id="exampleFormControlFile1">
  </div>
  <div class="col-md-6 mb-3">
      <label for="validationCustom05">Facebook Profile Name: (for Groupchat)</label>
      <input type="text" class="form-control" name="fb_name" id="validationCustom05" placeholder="Facebook Profile Name" value="<?php
if (isset($_GET['fb_name']))
{
echo $_GET['fb_name'];
}
?>" >
     
    </div>
  

      </label>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      <div class="form-step">
      <div class="form-group">
          <label for="username">Username</label>
          <input type="username"class="form-control" name="username" id="username" value="<?php
if (isset($_GET['username']))
{
echo $_GET['username'];
}
?>"/>
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input class="form-control" type="password" name="password" id="password" />
        </div>
        <div class="form-group">
          <label for="cpassword">Confirm Password</label>
          <input class="form-control" type="password" name="cpassword" id="cpassword"  />
        </div>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <input type="submit" value="Submit" class="btn" />
        </div>
      </div>
      </fieldset>
    </form>
<!--enrollment end-->


<br><br>
    <div class="footer-bottom">
        <h3>Get connected with us on social network: </h3>
           
            <ul class="socials">
                <li><a href="#"><i class="fa fa-facebook"></i>&nbsp WWW.FACEBOOK/LNUCTE.COM</a></li>
                <li><a href="#"><i class="fas fa-envelope"></i>&nbsp LNU.CTE.R8@GMAIL.COM</a></li>
                <li><a href="#"><i class="fas fa-phone-square-alt"></i>&nbsp (+63) 916 383 7417 </a></li>
                <li><a href="#"><i class="fas fa-map-marker-alt"></i>&nbsp LEYTE NORMAL UNIVERSITY </a></li>
            </ul>
            <p>copyright &copy;2021 CTE-Reviewer Center</p>
        </div>
</body>
</html>
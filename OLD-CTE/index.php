<?php

session_start();
if(isset($_SESSION['username']) && (!isset($_SESSION['key']))){
   header('location:account.php?q=1');
}
else if(isset($_SESSION['username']) && isset($_SESSION['key']) && $_SESSION['key'] == '54585c506829293a2d4c3b68543b316e2e7a2d277858545a36362e5f39'){
   header('location:dash.php?q=0');
}
else{}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" href="image/logo.png" type="image/icon" sizes="18x18">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title> LNU-CTE Reviewer </title>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
 <link  rel="stylesheet" href="css/footer.css">
 <link rel="stylesheet" href="css/dashboard.css">
 <link  rel="stylesheet" href="css/font.css">
  <script src="js/bootstrap.min.js"  type="text/javascript"></script>
  <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="sweetalert2.all.min.js"></script>
  
<?php
if (@$_GET['w']) {
    echo '<script>alert("' . @$_GET['w'] . '");</script>';
}
?>


    
</head>
<style>
  
  .schedule{
  position: absolute;
  left: 67%;
  padding-top: 8%;
  
  }
  .welcome{
    position:absolute;
    top: 50%;
    left: 50%;
    transform:translate(-50%,-50%);
    font-style:  sans-serif;
    user-select: none;
  }
  .welcome h1{
    color: blue;
    font-size: 70px;
    font-weight: bold;
    width: 900px;
    text-align: center;

  }
  .welcome h2{
    color: yellow;
    font-size: 40px;
    font-weight: bold;
    width: 885px;
    margin-top: 10px;
    text-align: center;

  }
  .btns{
    position:absolute;
    top: 70%;
    left: 30%;
    font-style:  sans-serif;
    user-select: none;
  }
  </style>
  
<body>

<!--navbar start-->
<nav class="navbar navbar-expand-lg navbar-light  sticky-top" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="#"><img src="image/logo.png" alt="Logo" style="width:88px;">&nbsp  CTE- Reviewer Center </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 
  <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
    <ul class="navbar-nav ">
      <li class="nav-item ">
      <a class="nav-link" href="index.php"><i class="fas fa-home"></i>&nbsp Home</a>
      </li>
      &nbsp
      <li class="nav-item">
        <a class="nav-link" href="enrollment.php"><i class="fas fa-calendar-alt"></i>&nbsp Enrollment</a>
      </li>
      &nbsp
      <li class="nav-item">
      <a class="nav-link" href="enrollment_form.php"><i class="fas fa-question-circle"></i>&nbsp About</a>
      </li>
    </ul>
  </div>
</nav>
<!--navbar end-->

<!--login start-->



  
<!--admin start-->
<div class="welcome">
  <h1>Welcome to LNU</h1>
  <h2>Center for Teaching Excellence Review enter</h2>
  </div>
  <br><br>
  <div class="btns">
  <button type="button" class="btn btn-outline-primary btn-lg" data-toggle="modal" data-target="#exampleModal1">
  I am a Review Master
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #e3f2fd;">
        <h5 class="modal-title"  id="exampleModalLabel">Login Admin </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal"  method="post" action="admin.php?q=index.php">
<fieldset>
<div class="form-group">
  <label class="col-md-3 control-label">Username :</label>  
  <div class="col-md-12">
  <input type="text" name="uname" maxlength="20"  placeholder="Username" class="form-control"/> 
    
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label">Password :</label>
  <div class="col-md-12">
  <input type="password" name="password" maxlength="30" placeholder="Password" class="form-control"/>
    
  </div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-outline-primary"   name="login" value="Login">Procceed</button>
</fieldset>
</form>
      </div>
    </div>
  </div>

  </div>
<!--admin end-->

&nbsp &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp &nbsp


  <!--user start-->
  <button type="button" class="btn btn-outline-warning btn-lg" data-toggle="modal" data-target="#exampleModal2">
  I am a Reviewee
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #e3f2fd;">
        <h5 class="modal-title" id="exampleModalLabel">Login User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" action="login.php?q=index.php" method="POST">
<fieldset>
<div class="form-group">
  <label class="col-md-3 control-label" for="username">Username :</label>  
  <div class="col-md-12">
  <input id="username" name="username" placeholder="Username" class="form-control input-md" type="username">
    
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="password">Password :</label>
  <div class="col-md-12">
    <input id="password" name="password" placeholder="Enter your Password" class="form-control input-md" type="password">
    
  </div>
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button type="submit" id="btn"class="btn btn-outline-primary">Procceed</button>
    </fieldset>
</form>


      </div>
      
    </div>
  </div>
  
</div>
</div>
<!--user end-->


 <!--login end-->
 

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div class="footer-bottom">
        <h3>Get connected with us on social network: </h3>
           
            <ul class="socials">
                <li><a href="#"><i class="fa fa-facebook"></i>&nbsp WWW.FACEBOOK/LNUCTE.COM</a></li>
                <li><a href="#"><i class="fas fa-envelope"></i>&nbsp LNU.CTE.R8@GMAIL.COM</a></li>
                <li><a href="#"><i class="fas fa-phone-square-alt"></i>&nbsp (+63) 916 383 7417 </a></li>
                <li><a href="#"><i class="fas fa-map-marker-alt"></i>&nbsp LEYTE NORMAL UNIVERSITY </a></li>
            </ul>
            <p>copyright &copy;2021 CTE-Reviewer Center</p>
        </div>


</body>
</html>

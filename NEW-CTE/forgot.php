
<?php 
session_start();
$error = array();

require "mail.php";

	if(!$con = mysqli_connect("localhost","root","","cee_db")){

		die("could not connect");
	}

	$mode = "enter_email";
	if(isset($_GET['mode'])){
		$mode = $_GET['mode'];
	}

	//something is posted
	if(count($_POST) > 0){

		switch ($mode) {
			case 'enter_email':
				// code...
				$email = $_POST['email'];
				//validate email
				if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
					$error[] = "Please enter a valid email";
				}elseif(!valid_email($email)){
					$error[] = "That email was not found";
				}else{

					$_SESSION['forgot']['email'] = $email;
					send_email($email);
					
					header("Location: forgot.php?mode=enter_code");
					
					die;
				}
				break;

			case 'enter_code':
				// code...
				$code = $_POST['code'];
				$result = is_code_correct($code);

				if($result == "the code is correct"){

					$_SESSION['forgot']['code'] = $code;
					header("Location: forgot.php?mode=enter_password");
					die;
				}else{
					$error[] = $result;
				}
				break;

			case 'enter_password':
				// code...
				$password = $_POST['password'];
				$password2 = $_POST['password2'];

				if($password !== $password2){
					$error[] = "Passwords do not match";
				}elseif(!isset($_SESSION['forgot']['email']) || !isset($_SESSION['forgot']['code'])){
					header("Location: forgot.php");
					die;
				}else{
					
					save_password($password);
					if(isset($_SESSION['forgot'])){
						unset($_SESSION['forgot']);
					}
			header("Location: index.php");
					die;
				}
				break;
			
			default:
				// code...
				break;
		}
	}

	function send_email($email){
		
		global $con;

		$expire = time() + (60 * 1);
		$code = rand(10000,99999);
		$email = addslashes($email);

		$query = "insert into codes (email,code,expire) value ('$email','$code','$expire')";
		mysqli_query($con,$query);

		//send email here
		send_mail($email,'Password reset',"Your code is " . $code);
	}
	
	function save_password($password){
		
		global $con;

		password_verify($password, $hashed_password);
		$email = addslashes($_SESSION['forgot']['email']);

		$query = "update examinee_tbl set exmne_password = '$password' where exmne_email = '$email' limit 1";
		mysqli_query($con,$query);

	}
	
	function valid_email($email){
		global $con;

		$email = addslashes($email);

		$query = "select * from examinee_tbl  where exmne_email = '$email' limit 1";		
		$result = mysqli_query($con,$query);
		if($result){
			if(mysqli_num_rows($result) > 0)
			{
				return true;
 			}
		}

		return false;

	}

	function is_code_correct($code){
		global $con;

		$code = addslashes($code);
		$expire = time();
		$email = addslashes($_SESSION['forgot']['email']);

		$query = "select * from codes where code = '$code' && email = '$email' order by id desc limit 1";
		$result = mysqli_query($con,$query);
		if($result){
			if(mysqli_num_rows($result) > 0)
			{
				$row = mysqli_fetch_assoc($result);
				if($row['expire'] > $expire){

					return "the code is correct";
				}else{
					return "the code is expired";
				}
			}else{
				return "the code is incorrect";
			}
		}

		return "the code is incorrect";
	}

	
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>CTE- Reviewer Center</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="login-ui/image/png" href="login-ui/images/logo.png"/>
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/util.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/main.css">
	<script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
	
	
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url(login-ui/images/back.jpg);  background-position: center; background-size: cover;  ">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(login-ui/images/index.png);">
					<span class="login100-form-title-1">
						forgot Password
					</span>
				</div>

		<?php 

			switch ($mode) {
				case 'enter_email':
					// code...
					?>
					<form method="post" id="examineeLoginFrm" class="login100-form validate-form">
							
							<h3>Enter your email below</h3>
							<span style="font-size: 12px;color:red;">
							<?php 
								foreach ($error as $err) {
									// code...
									echo $err . "<br>";
								}
							?>
							</span>
							
						<div class="wrap-input100 email-input m-b-26" >
						<span class="label-input100"><i class="fas fa-paper-plane" style="font-size:30px"></i></span>
						<input  class="input100" class="textbox" type="email" name="email" placeholder="Email">
						<span class="focus-input100"></span>
					    </div>
							
						

							<button  type="submit" value="Next" class="login100-form-btn"><i class="fas fa-shoe-prints"></i>&nbsp&nbspNext
						</button> &nbsp&nbsp &nbsp&nbsp &nbsp&nbsp
							<div><a href="/NEW-CTE" class="login100-form-btn"><i class="fas fa-sign-in-alt"></i>&nbsp&nbspLogin</a></div>
							
						</form>
					<?php				
					break;

				case 'enter_code':
					// code...
					?>
						<form method="post" id="examineeLoginFrm" class="login100-form validate-form">
							<h3>Enter your the code sent to your email</h3>
							<span style="font-size: 12px;color:red;">
							<?php 
								foreach ($error as $err) {
									// code...
									echo $err . "<br>";
								}
							?>
							</span>
						<div class="wrap-input100 code-input m-b-26" >
						<span class="label-input100"><i class="fas fa-code" style="font-size:30px"></i></span>
						<input  class="input100" class="textbox" type="text" name="code"  placeholder="Enter the code..">
						<span class="focus-input100"></span>
					    </div>
							

							<button  type="submit" value="Next" class="login100-form-btn"><i class="fas fa-shoe-prints"></i>&nbsp&nbspNext
						</button> &nbsp&nbsp &nbsp&nbsp &nbsp&nbsp
							<div><a href="/NEW-CTE" class="login100-form-btn"><i class="fas fa-sign-in-alt"></i>&nbsp&nbspLogin</a></div>
						</form>
					<?php
					break;

				case 'enter_password':
					// code...
					?>
						<form method="post" id="examineeLoginFrm" class="login100-form validate-form"> 
							<h3>Enter your new password</h3><br>
							<span style="font-size: 12px;color:red;">
							<?php 
								foreach ($error as $err) {
									// code...
									echo $err . "<br>";
								}
							?>
							</span>

							<div class="wrap-input100 code-input m-b-26" >
						    <span class="label-input100"></span>
						    <span class="label-input100"><i class="fas fa-unlock" style="font-size:30px"></i></span>
							<input class="input100" type="text"  name="password" placeholder="Password">
							<span class="focus-input100"></span>
					        </div>

					        <div class="wrap-input100 code-input m-b-26" >
						    <span class="label-input100"></span>
						    <span class="label-input100"><i class="fas fa-unlock" style="font-size:30px"></i></span>
							<input class="input100" type="password" id="id_password" name="password2" placeholder="Retype Password">
							<i class="far fa-eye fa-lg" id="togglePassword" style="margin-left: 330px; position:absolute; top: 40%;  cursor: pointer;"></i>
							<span class="focus-input100"></span>
					        </div>
							

							<button  type="submit" value="Next" class="login100-form-btn"><i class="fas fa-shoe-prints"></i>&nbsp&nbspNext
						</button> &nbsp&nbsp &nbsp&nbsp &nbsp&nbsp
							<div><a href="/NEW-CTE" class="login100-form-btn"><i class="fas fa-sign-in-alt"></i>&nbsp&nbspLogin</a></div>
						</form>
					<?php
					break;
				
				default:
					// code...
					break;
			}

		?>


    <script src="login-ui/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="login-ui/vendor/animsition/js/animsition.min.js"></script>
	<script src="login-ui/vendor/bootstrap/js/popper.js"></script>
	<script src="login-ui/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="login-ui/vendor/select2/select2.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/moment.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="login-ui/vendor/countdowntime/countdowntime.js"></script>
	<script src="login-ui/js/main.js"></script>
<script>const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#id_password');
 
  togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});</script>


</body>
</html>
<style>
    html {
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }
   </style> 

<div class="app-main__outer">
    <div id="refreshData">
            
    </div>

   <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--========== BOX ICONS ==========-->
        <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>

        <!--========== CSS ==========-->
        <link rel="stylesheet" href="assets1/css/styles.css">

    </head>


    <body>

      <!--========== BOX ICONS ==========-->
        <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>

        <!--========== CSS ==========-->
        <link rel="stylesheet" href="assets1/css/styles.css">





        <!--========== SCROLL TOP ==========-->
        <a href="#" class="scrolltop" id="scroll-top" style="background-color: #008CBA;">
            <i class='bx bx-chevron-up scrolltop__icon'></i>
        </a>






                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">

                        <li><i class='bx bx-dashboard change-theme' id="theme-button"></i></li>
                    </ul>
                </div>

        <main class="l-main" >
            <!--========== HOME ==========-->
            <section class="home" id="home">
                <div class="home__container bd-container bd-grid">
                    <div class="home__data">
                        <h1 class="home__title" style="color:blue">LNU-CTE</h1>
                        <h2 class="home__subtitle">Traning & Review <br> Center.</h2>
                    </div>
                    <div class="slide first">
                    <img src="assets1/img/cc.jpg" alt="" class="about__img">
        </div>
                </div>
            </section>
            
            <!--========== ABOUT ==========-->
            <section class="about section bd-container" id="about">
                <div class="about__container  bd-grid">
                    <div class="about__data">
                        <span class="section-subtitle about__initial" style="color:blue">About us</span>
                        <h2 class="section-title about__initial">We CTE <br> Center for Teaching and Review Center</h2>
                        <p class="about__description">Is a education network that helps connect all learners with the people 
                        and resources needed to reach their full potential.</p>
                    </div>

                    <img src="assets1/img/home.jpg" alt="" class="about__img">
                </div>
            </section>

            <!--========== SERVICES ==========-->
            <section class="services section bd-container" id="services">
                <span class="section-subtitle"style="color:blue">Offering</span>
                <h2 class="section-title">Our services</h2>

                <div class="services__container  bd-grid">
                    <div class="services__content">
                       <img src="https://img.icons8.com/external-inipagistudio-mixed-inipagistudio/64/000000/external-enrollment-online-crash-course-inipagistudio-mixed-inipagistudio.png"/>


                        <h3 class="services__title">Enrollment</h3>
                        <p class="services__description">Amidst the current situation that the world is facing, we offer you an enrollment that at its most convenient way possible through online.</p>
                    </div>

                    <div class="services__content">
                        <img src="https://img.icons8.com/external-inipagistudio-lineal-color-inipagistudio/64/000000/external-payment-travel-app-inipagistudio-lineal-color-inipagistudio.png"/>
                        <h3 class="services__title">E-Payment</h3>
                        <p class="services__description">Along with this, we also offer e-payment for any possible money transactions during enrollment.</p>
                    </div>

                    <div class="services__content">
                        <img src="https://img.icons8.com/external-kiranshastry-lineal-kiranshastry/64/000000/external-chat-management-kiranshastry-lineal-kiranshastry-1.png"/>
                        <h3 class="services__title">Inquries</h3>
                        <p class="services__description">You can also send inquiries to us.</p>
                    </div>
                </div>
            </section>

            <!--========== CONTACT US ==========-->
            <section class="contact section bd-container" id="contact">
                <div class="contact__container bd-grid">
                    <div class="contact__data">
                        <span class="section-subtitle contact__initial" style="color:blue">Let's talk</span>
                        <h2 class="section-title contact__initial">Contact us</h2>
                        <p class="contact__description">If you want to enroll, please send us email or contact us and we will attend you quickly, with our chat service.</p>
                    </div>

                    <div class="contact__button">
                        <a style="background-color: #008CBA;" href="https://web.facebook.com/lnucte/about/?ref=page_internal" class="button">Contact us now</a>
                    </div>
                </div>
            </section>
        </main>

        <!--========== FOOTER ==========-->
        <footer class="footer section bd-container">
            <div class="footer__container bd-grid">
                <div class="footer__content">
                    <a href="#" class="footer__logo" style="color:blue">LNU-CTE</a>
                    <span class="footer__description">Training & Review Center</span>
                    <div>
                        <a href="#" class="footer__social"><i class='bx bxl-facebook'></i></a>
                        <a href="#" class="footer__social"><i class='bx bxl-instagram'></i></a>
                        <a href="#" class="footer__social"><i class='bx bxl-twitter'></i></a>
                    </div>
                </div>

                <div class="footer__content">
                    <h3 class="footer__title">Services</h3>
                    <ul>
                        <li><a href="#" class="footer__link">Online Enrollment</a></li>
                        <li><a href="#" class="footer__link">E-payment</a></li>
                        <li><a href="#" class="footer__link">Inqueries</a></li>
                        <li><a href="#" class="footer__link">Online learning</a></li>
                    </ul>
                </div>

                <div class="footer__content">
                    <h3 class="footer__title">Information</h3>
                    <ul>
                        <li><a href="#" class="footer__link">Event</a></li>
                        <li><a href="#" class="footer__link">Contact us</a></li>
                        <li><a href="#" class="footer__link">Privacy policy</a></li>
                        <li><a href="#" class="footer__link">Terms of services</a></li>
                    </ul>
                </div>

                <div class="footer__content">
                    <h3 class="footer__title">Adress</h3>
                    <ul>
                        <li>Paterno st. Tacloban City</li>
                        <li>LEYTE NORMAL UNIVERSITY</li>
                        <li>(+63) 916 383 7417</li>
                        <li>LNU-CTE@email.com</li>
                    </ul>
                </div>
            </div>

            <p class="footer__copy">&#169; 2021 LNU-CTE. All right reserved</p>
        </footer>

        <!--========== SCROLL REVEAL ==========-->
        <script src="https://unpkg.com/scrollreveal"></script>

        <!--========== MAIN JS ==========-->
        <script src="assets1/js/main.js"></script>
    </body>
</html>


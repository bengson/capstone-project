<?php
include 'knn.php';
?>
<link href="css/mycss.css" rel="stylesheet">
<?php
$examId = $_GET['id'];
$selExam = $conn->query("SELECT * FROM exam_tbl WHERE ex_id='$examId' ")->fetch(PDO::FETCH_ASSOC);

?>

<style>
    th {
        text-align: center;
    }

    .table-responsive thead tr {
        background-color: #e1ad01;
        color: #ffffff;


        overflow: scroll;
        overflow-x: hidden;
    }

    ::-webkit-scrollbar {
        width: 0px;
    }

    ::-webkit-scrollbar-thumb {
        background: #FF0000;
    }
</style>
<div class="app-main__outer">
    <div class="app-main__inner">
        <div id="refreshData">

            <div class="col-md-12 ">
                <div class="app-page-title ">
                    <div class="page-title-wrapper ">
                        <div class="page-title-heading ">
                            <div>
                                <?php echo $selExam['ex_title']; ?>
                                <div class="page-title-subheading">
                                    <?php echo $selExam['ex_description']; ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row col-md-12">
                    <h1 class="text-primary">RESULTS</h1>
                </div>

                <div class="row col-md-6 float-left">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Your Answer's</h5>
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                                <?php
                                //$selQuest = $conn->query("SELECT * FROM exam_question_tbl WHERE exam_id='$examId' ORDER BY ques_category  ");
                                $selQuest = $conn->query("SELECT * FROM exam_question_tbl eqt  JOIN exam_answers ea ON eqt.eqt_id = ea.quest_id WHERE  eqt.exam_id='$examId' AND ea.axmne_id='$exmneId' ORDER BY eqt.ques_category  ");
                                $i = 1;
                                $ques_category_id = null;
                                while ($selQuestRow = $selQuest->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <?php
                                    if ($ques_category_id != $selQuestRow['ques_category']) {
                                        $ques_category_id = $selQuestRow['ques_category'];
                                        $ques_category = "";
                                        switch ($ques_category_id) {
                                            case 1:
                                                $ques_category = 'Test I English';
                                                break;
                                            case 2:
                                                $ques_category = 'Test II Filipino';
                                                break;
                                            case 3:
                                                $ques_category = 'Test III Math';
                                                break;
                                            case 4:
                                                $ques_category = 'Test IV Science';
                                                break;
                                        }
                                        echo "<tr style=\"background-color:blue;color:white;\"><th>$ques_category</th></tr>";
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <b>
                                                <p><?php echo $i++; ?> .) <?php echo $selQuestRow['exam_question']; ?></p>
                                            </b>
                                            <label class="pl-4 text-success">
                                                Answer :
                                                <?php
                                                if ($selQuestRow['exam_answer'] != $selQuestRow['exans_answer']) { ?>
                                                    <span style="color:red"><?php echo $selQuestRow['exans_answer']; ?></span>
                                                <?PHP } else { ?>
                                                    <span class="text-success"><?php echo $selQuestRow['exans_answer']; ?></span>
                                                <?php }
                                                ?>
                                            </label>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-header">Your Result
                        </div>
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                                <thead>
                                    <tr>
                                        <th>Score</th>
                                        <th>Percentage</th>
                                        <th>Cluster</th>
                                        <th>Recommendation</th>
                                        <!-- <th width="10%"></th> -->
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $selScore = $conn->query("SELECT * FROM exam_question_tbl eqt INNER JOIN exam_answers ea ON eqt.eqt_id = ea.quest_id AND eqt.exam_answer = ea.exans_answer  WHERE ea.axmne_id='$exmneId' AND ea.exam_id='$examId' AND ea.exans_status='new' ");
                                    ?>
                                    <th>
                                        <?php echo $selScore->rowCount(); ?>
                                        <?php
                                        $over  = $selExam['ex_questlimit_display'];
                                        ?>
                                        / <?php echo $over; ?>
                                    </th>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $selScore = $conn->query("SELECT * FROM exam_question_tbl eqt INNER JOIN exam_answers ea ON eqt.eqt_id = ea.quest_id AND eqt.exam_answer = ea.exans_answer  WHERE ea.axmne_id='$exmneId' AND ea.exam_id='$examId' AND ea.exans_status='new' ");
        ?>
        <th>
            <?php
            $score = $selScore->rowCount();
            $ans = $score / $over * 100;
            echo number_format($ans, 2);
            echo "%";

            ?>
        </th>
    </div>
</div>
</div>
</div>
</div>


<th>
    <?php
    //$ans = $score;                                   

    if ($ans >= 1 && $ans < 33) {
        echo 'C';
    } elseif ($ans >= 33 && $ans < 66) {
        echo 'B';
    } elseif ($ans >= 66 && $ans <= 100) {
        echo 'A';
    }

    ?>
</th>
<th>
    <?php
    $selScores = $conn->query("SELECT * FROM `score_per_category_with_answer_checking` where exmne_id='$exmneId' and ex_id='$examId'  ");
    $ItemsPerCategory = $conn->query("SELECT COUNT(exam_question) as category_over, exam_id, ques_category FROM exam_question_tbl where exam_id='$examId' GROUP BY  exam_id, ques_category ;  ");
    $math        = 0;
    $filipino    = 0;
    $english    = 0;
    $science    = 0;

    $ItemsPerCategory = $ItemsPerCategory->fetchAll(PDO::FETCH_ASSOC);
    while ($selScore = $selScores->fetch(PDO::FETCH_ASSOC)) {

        foreach ($ItemsPerCategory as $ItemPerCategory) {
            if ($ItemPerCategory['ques_category'] == $selScore['ques_category']) {
                $scorePercentage = $selScore['category_score'] / $ItemPerCategory['category_over'];

                $rawScore = round($scorePercentage * 10);
                if ($rawScore > 0) {
                    $rawScore--;
                }

                $category_name = "";
                switch ($selScore['ques_category']) {
                    case 1:
                        $english = $rawScore;
                        break;
                    case 2:
                        $filipino = $rawScore;
                        break;
                    case 3:
                        $math = $rawScore;
                        break;
                    case 4:
                        $science = $rawScore;
                        break;
                }
            }
        }
    }
    //echo "english: $english filipino: $filipino math: $math science: $science";
    echo "You need to review: <b>{$classifier->predict([$math, $filipino, $english, $science])}</b>";

    ?>
</th>
</div>
</div>
</div>
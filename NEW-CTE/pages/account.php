

<style>
    th {
        text-align: center;
    }

    .table-responsive thead tr {
        background-color: #e1ad01;
        color: #ffffff;


        overflow: scroll;
        overflow-x: hidden;
    }

    ::-webkit-scrollbar {
        width: 0px;
    }

    ::-webkit-scrollbar-thumb {
        background: #FF0000;
    }
</style>
<div class="app-main__outer">
    <div class="app-main__inner">
<div class="col-md-12">
          <div class="form-group">
          <h1><img src="assets/images/use.png" height="90" width="90" ></i>&nbsp<?php echo strtoupper($selExmneeData['exmne_fullname']); ?> </h1><br>
            
          <h3> Personal Information</h3><br>
          <div class="form-row">
          <div class="form-group col-md-6">
        <legend>Fullname</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exFullname" class="form-control" required="" value="<?php echo $selExmneeData['exmne_fullname']; ?>" >
     </div>
     <div class="form-group col-md-6">
        <legend>Middle Name</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exMiddle" class="form-control" required="" value="<?php echo $selExmneeData['exmne_mname']; ?>" >
     </div>
     </div> 
     <div class="form-row">
             <div class="form-group col-md-6">
        <legend>Birthdate</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exBirthdate" class="form-control" required="" value="<?php echo $selExmneeData['exmne_birthdate']; ?>" >
     </div>
     <div class="form-group col-md-6">
     <legend>Gender</legend>
      <input type="" name="exGender" class="form-control" required="" value="<?php echo $selExmneeData['exmne_gender']; ?>" >
     </div>
     </div>

             <h3> Contact Information</h3><br>

             <div class="form-row">
             <div class="form-group col-md-6">
        <legend>Contact Number</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exContact" class="form-control" required="" value="<?php echo $selExmneeData['exmne_contact']; ?>" >
     </div>
</div>
            
             <h3> School Information</h3><br>

             <div class="form-row">
             <div class="form-group col-md-6">
        <legend>School Graduate</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exGrad" class="form-control" required="" value="<?php echo $selExmneeData['exmne_schlgrad']; ?>" >
     </div>
     <div class="form-group col-md-6">
     <legend>Year Graduate</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exYearGrad" class="form-control" required="" value="<?php echo $selExmneeData['exmne_yrgrad']; ?>" >
     </div>
</div>
<div class="form-row">
             <div class="form-group col-md-6">
        <legend>Academic Award</legend>
         <input type="" name="exAcad" class="form-control" required="" value="<?php echo $selExmneeData['exmne_acad']; ?>" >
     </div>
     <div class="form-group col-md-6">
            <label>First Taker</label>
            <input type="" name="exTake" class="form-control" required="" value="<?php echo $selExmneeData['exmne_take']; ?>" >
          </div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
        <legend>Majorship</legend>
         <input type="" name="exMajorship" class="form-control" required="" value="<?php echo $selExmneeData['exmne_majorship']; ?>" >
     </div>

     <div class="form-group col-md-6">
        <legend>Course</legend>

        <?php 
            $exmneCourse = $selExmneeData['exmne_course'];
            $selCourse = $conn->query("SELECT * FROM course_tbl WHERE cou_id='$exmneCourse' ")->fetch(PDO::FETCH_ASSOC);
         ?>
          <input type="" name="exCourse" class="form-control" required="" value="<?php echo $selCourse['cou_name']; ?>" >
        
     </div>
     </div>

     <h3> Account Information</h3><br>

<div class="form-row">
<div class="form-group col-md-6 ">
<legend>Email</legend>
<input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
<input type="" name="exEmail" class="form-control" required="" value="<?php echo $selExmneeData['exmne_email']; ?>" >
</div>
<div class="form-group col-md-6">
<legend>Password</legend>
<input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
<input type="password" id="id_password" name="exPass" class="form-control" required="" value="<?php echo $selExmneeData['exmne_password']; ?>" >
<i class="far fa-eye fa-lg" id="togglePassword" style="margin-left: 530px; position:absolute; top: 60%;  cursor: pointer;"></i>
</div>

  
</div>
<br>
  <div class="d-grid gap-2 d-md-flex justify-content-md-end">
          <button type="submit"  class="btn btn-outline-primary  me-md-2" data-toggle="modal" data-target="#userModal">Update Account</button>
          </div>
        </div>
      </div>

  </div>

  </div>
  <script>const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#id_password');
 
  togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});</script>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=100, initial-scale=1.0">
	<title>Ajax trial</title>
</head>

<body>

	<button onclick="getKnn()">Submit</button>

	<br>

	<h1 id="recommendedSubject">Press submit</h1>

	<script src="js/jquery.js"></script>
	<script>
		$("#scoresForm").submit(function(e) {
			e.preventDefault();
		});

		function getKnn() {
			$.get('knn.php', "math=6&science=3&filipino=4&english=0").done(function(data) {
				$("#recommendedSubject").html(data);
			});
		}
	</script>
</body>

</html>
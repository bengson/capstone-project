<!-- Modal feedback-->
<div class="modal fade" id="feedbacksModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
   
     <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Submit Feedbacks</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="form-group">
            <form class="refreshFrm" id="addFeebacks" action="updateuser.php" method="post">
            <label>Feedback AS</label><br>
            <?php 
               $selMe = $conn->query("SELECT * FROM examinee_tbl WHERE exmne_id='$exmneId' ")->fetch(PDO::FETCH_ASSOC);
             ?>
            <input type="radio" name="asMe" value="<?php echo $selMe['exmne_fullname']; ?>"> <?php echo $selMe['exmne_fullname']; ?> <br>
            <input type="radio" name="asMe" value="Anonymous"> Anonymous
            
          </div>
          <div class="form-group">
           <textarea name="myFeedbacks" class="form-control" rows="3" placeholder="Input your feedback here.."></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-outline-primary">Add Now</button>
      </div>
    </div>
   </form>
  </div>
</div>





<!-- Modal User -->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
   <form class="refreshFrm" id="updateAccountFrm" method="post">
     <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">MY ACCOUNT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="form-group">

            <?php 

               $selExmne = $conn->query("SELECT * FROM examinee_tbl WHERE exmne_id='$exmneId' ")->fetch(PDO::FETCH_ASSOC);
             ?>
          <h1><img src="assets/images/use.png" height="90" width="90" ></i>&nbsp<?php echo strtoupper($selExmne['exmne_fullname']); ?> </h1><br>
             
          
          
          <h3> Personal Information</h3><br>
          <div class="form-row">
          <div class="form-group col-md-6">
        <legend>Fullname</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exFullname" class="form-control" required="" value="<?php echo $selExmne['exmne_fullname']; ?>" >
     </div>
     <div class="form-group col-md-6">
        <legend>Middle Name</legend>
        <input type="" name="exMiddle" class="form-control" required="" value="<?php echo $selExmne['exmne_mname']; ?>" >
     </div>
     </div> 
     <div class="form-row">
             <div class="form-group col-md-6">
        <legend>Birthdate</legend>
        <input type="" name="exBirthdate" class="form-control" required="" value="<?php echo $selExmne['exmne_birthdate']; ?>" >
     </div>
     <div class="form-group col-md-6">
     <legend>Gender</legend>
        <select class="form-control" name="exGender">
          <option value="<?php echo $selExmne['exmne_gender']; ?>"><?php echo $selExmne['exmne_gender']; ?></option>
          <option value="M">Male</option>
          <option value="F">Female</option>
        </select>
     </div>
     </div>

             <h3> Contact Information</h3><br>

             <div class="form-row">
             <div class="form-group col-md-6">
        <legend>Contact Number</legend>
        <input type="" name="exContact" class="form-control" required="" value="<?php echo $selExmne['exmne_contact']; ?>" >
     </div>
     </div>

            

             <h3> School Information</h3><br>

             <div class="form-row">
             <div class="form-group col-md-6">
        <legend>School Graduate</legend>
        <input type="" name="exGrad" class="form-control" required="" value="<?php echo $selExmne['exmne_schlgrad']; ?>" >
     </div>
     <div class="form-group col-md-6">
     <legend>Year Graduate</legend>
        <input type="" name="exYearGrad" class="form-control" required="" value="<?php echo $selExmne['exmne_yrgrad']; ?>" >
     </div>
</div>
<div class="form-row">
             <div class="form-group col-md-6">
        <legend>Academic Award</legend>
        <select class="form-control" name="exAcad">
        <option value="<?php echo $selExmne['exmne_acad']; ?>"><?php echo $selExmne['exmne_acad']; ?></option>
            
              <option value="C">Cumlaude</option>
              <option value="MC"> Magna Cumlaude</option>
              <option value="SC"> Summa Cumlaude</option>
              <option value="N">None</option>
            </select>
     </div>
     <div class="form-group col-md-6">
            <label>First Taker</label>
            <select class="form-control" name="exTake" id="exTake">
            <option value="<?php echo $selExmne['exmne_take']; ?>"><?php echo $selExmne['exmne_take']; ?></option>
           <option  value="Yes">Yes</option>
           <option  value="No">No</option>
            </select>
          </div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
        <legend>Majorship</legend>
        <select class="form-control" name="exMajorship">
          <option value="<?php echo $selExmne['exmne_majorship']; ?>"><?php echo $selExmne['exmne_majorship']; ?></option>
          <option value="English">English</option>
      <option value="Filipino">Filipino</option>
      <option value="Mathemathics">Mathemathics</option>
	  <option value="Social Science">Social Science</option>
	  <option value="Biological Science">Biological Science</option>
	  <option value="Physical Science">Physical Science</option>
	  <option value="MAPEH">MAPEH</option>
	  <option value="TLE">TLE</option>
	  <option value="Fishery Arts">Fishery Arts</option>
        </select>
     </div>

     <div class="form-group col-md-6">
        <legend>Course</legend>
        <?php 
            $exmneCourse = $selExmne['exmne_course'];
            $selCourse = $conn->query("SELECT * FROM course_tbl WHERE cou_id='$exmneCourse' ")->fetch(PDO::FETCH_ASSOC);
         ?>
         <select class="form-control" name="exCourse">
           <option value="<?php echo $exmneCourse; ?>"><?php echo $selCourse['cou_name']; ?></option>
           <?php 
             $selCourse = $conn->query("SELECT * FROM course_tbl WHERE cou_id!='$exmneCourse' ");
             while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) { ?>
              <option value="<?php echo $selCourseRow['cou_id']; ?>"><?php echo $selCourseRow['cou_name']; ?></option>
            <?php  }
            ?>
         </select>
     </div>
     </div>

     <h3> Account Information</h3><br>

<div class="form-row">
<div class="form-group col-md-6 ">
<legend>Email</legend>
<input type="" name="exEmail" class="form-control" required="" value="<?php echo $selExmne['exmne_email']; ?>" >
</div>
<div class="form-group col-md-6">
<legend>Password</legend>
<input type="text" id="id_password" name="exPass" class="form-control" required="" value="<?php echo $selExmne['exmne_password']; ?>" >

</div>
</div>
 
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-outline-primary">Save Changes</button>
      </div>
      
    </div>
   </form>
  </div>
</div>
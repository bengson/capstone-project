-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2021 at 11:47 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cee_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_acc`
--

CREATE TABLE `admin_acc` (
  `admin_id` int(11) NOT NULL,
  `admin_user` varchar(1000) NOT NULL,
  `admin_pass` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_acc`
--

INSERT INTO `admin_acc` (`admin_id`, `admin_user`, `admin_pass`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `code` varchar(5) NOT NULL,
  `expire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `email`, `code`, `expire`) VALUES
(0, 'justineprensica@gmail.com', '11260', 1635668786),
(0, 'justineprensica@gmail.com', '21501', 1635668817),
(0, 'justineprensica@gmail.com', '60195', 1635669165),
(0, 'justineprensica@gmail.com', '37138', 1635671843),
(0, 'justineprensica@gmail.com', '44524', 1635671944),
(0, 'justineprensica@gmail.com', '16830', 1635676998),
(0, 'justineprensica@gmail.com', '26808', 1635679662),
(0, 'markjustinebengson.lnu@gmail.com', '91144', 1635740429),
(0, 'markjustinebengson.lnu@gmail.com', '81364', 1635741813),
(0, 'markjustinebengson.lnu@gmail.com', '38544', 1635742222),
(0, 'markjustinebengson.lnu@gmail.com', '95707', 1635743828),
(0, 'markjustinebengson.lnu@gmail.com', '63588', 1635744106);

-- --------------------------------------------------------

--
-- Table structure for table `course_tbl`
--

CREATE TABLE `course_tbl` (
  `cou_id` int(11) NOT NULL,
  `cou_name` varchar(1000) NOT NULL,
  `cou_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_tbl`
--

INSERT INTO `course_tbl` (`cou_id`, `cou_name`, `cou_created`) VALUES
(26, 'BSED', '2021-09-22 03:04:13'),
(65, 'BEED', '2021-09-22 03:03:42'),
(67, 'BEED (SUPPLEMENTAL)', '2021-09-22 03:05:34'),
(68, 'BSED (TCP)', '2021-09-22 03:06:12');

-- --------------------------------------------------------

--
-- Table structure for table `examinee_tbl`
--

CREATE TABLE `examinee_tbl` (
  `exmne_id` int(11) NOT NULL,
  `exmne_fullname` varchar(1000) NOT NULL,
  `exmne_mname` varchar(255) NOT NULL,
  `exmne_contact` varchar(50) NOT NULL,
  `exmne_course` varchar(1000) NOT NULL,
  `exmne_gender` varchar(1000) NOT NULL,
  `exmne_studentid` varchar(255) NOT NULL,
  `exmne_schlgrad` varchar(255) NOT NULL,
  `exmne_yrgrad` varchar(255) NOT NULL,
  `exmne_acad` int(255) NOT NULL,
  `exmne_take` varchar(255) NOT NULL,
  `exmne_majorship` varchar(255) NOT NULL,
  `exmne_payment` varchar(255) NOT NULL,
  `exmne_pic` varchar(50) NOT NULL,
  `exmne_fb` varchar(255) NOT NULL,
  `exmne_birthdate` varchar(1000) NOT NULL,
  `exmne_year_level` varchar(1000) NOT NULL,
  `exmne_email` varchar(1000) NOT NULL,
  `exmne_password` varchar(1000) NOT NULL,
  `exmne_status` varchar(1000) NOT NULL DEFAULT 'active',
  `status` varchar(255) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `codee` varchar(10) NOT NULL,
  `updated_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examinee_tbl`
--

INSERT INTO `examinee_tbl` (`exmne_id`, `exmne_fullname`, `exmne_mname`, `exmne_contact`, `exmne_course`, `exmne_gender`, `exmne_studentid`, `exmne_schlgrad`, `exmne_yrgrad`, `exmne_acad`, `exmne_take`, `exmne_majorship`, `exmne_payment`, `exmne_pic`, `exmne_fb`, `exmne_birthdate`, `exmne_year_level`, `exmne_email`, `exmne_password`, `exmne_status`, `status`, `code`, `codee`, `updated_time`) VALUES
(84, 'Joshua', 'Andriano', '09658430979', '67', 'M', ' 1802945 ', 'lnu', '2012-2013', 0, 'Yes', 'Physical Science', 'Gcash', 'ac0d1451230de9bbd1bd036e3bb1a384 (1).jpg', 'MarkJustin', '', '', 'josh123@gmail.com', '123', 'active', 'approved', NULL, '', '2021-10-28 22:31:10'),
(85, 'Mark Justine', 'Prensica', '09462135584', '26', 'M', ' 1802587 ', 'Leyte Normal', '2021', 0, 'Yes', 'MAPEH', 'Palawan', 'bb.jpg', 'Justine', '', '', 'justineprensica@gmail.com', '123', 'active', 'approved', NULL, 'ATHWJUCP4R', '2021-10-31 10:42:54'),
(90, 'win', 'P', '23131231', '26', 'M', ' 12314124 ', 'Leyte Normal', '2021', 0, 'Yes', 'TLE', 'Palawan', '6fdd0f095f6b5c2f8d37afd94ffba7af.jpg', 'winwin', '2021-11-02', '', 'win@gmail.com', '124', 'active', 'approved', NULL, '', '2021-10-28 22:31:10'),
(93, 'Luke Bengson', 'P', '09462135584', '26', 'M', ' 123456 ', 'Leyte Normal', '2021', 0, 'Yes', 'MAPEH', 'Palawan', '7ee768e122e073675db4bccd270a7b08.jpg', 'Luke Bengson', '1998-02-01', '', 'luke@gmail.com', '12345', 'active', 'approved', NULL, '', '2021-10-28 22:31:10'),
(95, 'joshua', 'biron', '0927367387', '67', 'M', ' 19272 ', 'Leyte Normal', '2018', 0, 'Yes', 'Fishery Arts', 'Gcash', '6fdd0f095f6b5c2f8d37afd94ffba7af.jpg', 'asd', '1999-12-11', '', 'joshuer231@gmail.com', 'asd', 'active', 'approved', NULL, '', '2021-10-28 22:31:10'),
(96, 'Rap abac', 'B', '09462135584', '68', 'F', ' 12345 ', 'Leyte Normal', '2021', 0, 'Yes', 'TLE', 'Gcash', '7ee768e122e073675db4bccd270a7b08.jpg', 'rapp', '1998-02-01', '', 'rapp@gmail.com', '12345', 'active', 'approved', NULL, '', '2021-10-28 22:31:10'),
(99, 'Mark mac', 'P', '09185455050', '68', 'M', ' 2313123 ', 'Leyte Normal', '2021', 0, 'Yes', 'TLE', 'Palawan', 'updates.jpeg', 'mac', '2002-02-01', '', 'markjustinebengson.lnu@gmail.com', 'Password', 'active', 'approved', NULL, '', '2021-11-01 05:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `exam_answers`
--

CREATE TABLE `exam_answers` (
  `exans_id` int(11) NOT NULL,
  `axmne_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `quest_id` int(11) NOT NULL,
  `exans_answer` varchar(1000) NOT NULL,
  `exans_status` varchar(1000) NOT NULL DEFAULT 'new',
  `exans_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ques_category` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_answers`
--

INSERT INTO `exam_answers` (`exans_id`, `axmne_id`, `exam_id`, `quest_id`, `exans_answer`, `exans_status`, `exans_created`, `ques_category`) VALUES
(406, 86, 39, 79, '40', 'new', '2021-10-15 00:55:13', ''),
(407, 86, 39, 80, 'Calabao', 'new', '2021-10-15 00:55:13', ''),
(408, 86, 39, 78, 'random access memory', 'new', '2021-10-15 00:55:13', ''),
(409, 86, 39, 81, 'Water', 'new', '2021-10-15 00:55:13', ''),
(410, 86, 39, 82, '16', 'new', '2021-10-15 00:55:13', ''),
(411, 91, 39, 79, '22', 'new', '2021-10-15 01:02:56', ''),
(412, 91, 39, 81, 'Water', 'new', '2021-10-15 01:02:56', ''),
(413, 91, 39, 82, '22', 'new', '2021-10-15 01:02:56', ''),
(414, 91, 39, 78, 'random access memory', 'new', '2021-10-15 01:02:56', ''),
(415, 91, 39, 80, 'Aso', 'new', '2021-10-15 01:02:56', ''),
(416, 94, 39, 79, '40', 'new', '2021-10-21 03:02:52', ''),
(417, 94, 39, 78, 'random access memory', 'new', '2021-10-21 03:02:52', ''),
(418, 94, 39, 81, 'Water', 'new', '2021-10-21 03:02:52', ''),
(419, 94, 39, 82, '22', 'new', '2021-10-21 03:02:52', ''),
(420, 94, 39, 80, 'Kambing', 'new', '2021-10-21 03:02:52', ''),
(421, 86, 40, 85, '4', 'new', '2021-10-25 07:00:46', ''),
(422, 97, 40, 86, '6', 'new', '2021-10-25 07:05:09', ''),
(423, 97, 40, 87, 'a', 'new', '2021-10-25 07:05:09', ''),
(424, 97, 40, 85, '5', 'new', '2021-10-25 07:05:09', ''),
(425, 96, 42, 88, '0', 'new', '2021-10-26 11:12:14', ''),
(426, 96, 42, 89, 's', 'new', '2021-10-26 11:12:14', ''),
(427, 96, 43, 90, 'D', 'new', '2021-10-27 12:15:09', ''),
(428, 96, 43, 91, '10', 'new', '2021-10-27 12:15:09', ''),
(429, 84, 44, 92, 's', 'new', '2021-10-27 12:20:16', ''),
(430, 84, 44, 93, 'zz', 'new', '2021-10-27 12:20:16', ''),
(431, 95, 44, 93, 'bb', 'new', '2021-10-31 07:46:09', ''),
(432, 95, 44, 92, 's', 'new', '2021-10-31 07:46:09', '');

-- --------------------------------------------------------

--
-- Table structure for table `exam_attempt`
--

CREATE TABLE `exam_attempt` (
  `examat_id` int(11) NOT NULL,
  `exmne_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `examat_status` varchar(1000) NOT NULL DEFAULT 'used',
  `exam_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attempt`
--

INSERT INTO `exam_attempt` (`examat_id`, `exmne_id`, `exam_id`, `examat_status`, `exam_date`) VALUES
(79, 86, 39, 'used', ''),
(80, 91, 39, 'used', ''),
(81, 94, 39, 'used', ''),
(82, 86, 40, 'used', ''),
(83, 97, 40, 'used', ''),
(84, 96, 42, 'used', ''),
(85, 96, 43, 'used', ''),
(86, 84, 44, 'used', ''),
(87, 95, 44, 'used', '');

-- --------------------------------------------------------

--
-- Table structure for table `exam_question_tbl`
--

CREATE TABLE `exam_question_tbl` (
  `eqt_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_question` varchar(1000) NOT NULL,
  `exam_ch1` varchar(1000) NOT NULL,
  `exam_ch2` varchar(1000) NOT NULL,
  `exam_ch3` varchar(1000) NOT NULL,
  `exam_ch4` varchar(1000) NOT NULL,
  `exam_answer` varchar(1000) NOT NULL,
  `exam_status` varchar(1000) NOT NULL DEFAULT 'active',
  `exam_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ques_category` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_question_tbl`
--

INSERT INTO `exam_question_tbl` (`eqt_id`, `exam_id`, `exam_question`, `exam_ch1`, `exam_ch2`, `exam_ch3`, `exam_ch4`, `exam_answer`, `exam_status`, `exam_date`, `ques_category`) VALUES
(78, 39, 'RAM stand for?', 'random access memory', 'random affordable memory ', 'B', 'A', 'random access memory', 'active', '2021-10-15 00:49:03', '1'),
(79, 39, '20+20 =', '40', '22', '12', '55', '40', 'active', '2021-10-15 00:49:36', '3'),
(80, 39, 'Pambansang hayop?', 'Calabao', 'Kambing', 'Aso', 'Pusa', 'Calabao', 'active', '2021-10-15 00:50:45', '2'),
(81, 39, 'What is H2W0', 'Water', 'Air', 'Rock', 'heart', 'Water', 'active', '2021-10-15 00:52:31', '4'),
(82, 39, '8 + 8 =', '16', '22', '55', '12', '16', 'active', '2021-10-15 00:53:49', '3'),
(84, 39, '9+9 =', '18', '22', '3', '55', '18', 'active', '2021-10-15 01:32:32', '3'),
(85, 40, '2+2', '4', '5', '6', '7', '4', 'active', '2021-10-25 06:31:09', '3'),
(86, 40, '3+3', '6', '7', '8', '9', '6', 'active', '2021-10-25 06:31:49', '3'),
(87, 40, 'wadssad', 'q', 'w', 'a', 'v', 'q', 'active', '2021-10-25 06:59:17', '1'),
(88, 42, '0+0', '0', '2', '4', '1', '0', 'active', '2021-10-26 11:11:23', '3'),
(89, 42, 'qwewqe', 'q', 's', 'f', 'c', 'c', 'active', '2021-10-26 11:11:41', '1'),
(90, 43, 'ASDASDA', 'D', 'H', 'G', 'E', 'E', 'active', '2021-10-27 12:13:22', '1'),
(91, 43, '1+9', '10', '6', '4', '1', '10', 'active', '2021-10-27 12:13:47', '3'),
(92, 44, 'asdsadasd', 's', 'd', 'b', 'm', 'm', 'active', '2021-10-27 12:19:07', '4'),
(93, 44, 'asdasdasd', 'bb', 'dd', 'zz', 'ww', 'zz', 'active', '2021-10-27 12:19:32', '2');

-- --------------------------------------------------------

--
-- Table structure for table `exam_tbl`
--

CREATE TABLE `exam_tbl` (
  `ex_id` int(11) NOT NULL,
  `cou_id` int(11) NOT NULL,
  `ex_title` varchar(1000) NOT NULL,
  `ex_time_limit` varchar(1000) NOT NULL,
  `ex_questlimit_display` int(11) NOT NULL,
  `ex_description` varchar(1000) NOT NULL,
  `ex_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_tbl`
--

INSERT INTO `exam_tbl` (`ex_id`, `cou_id`, `ex_title`, `ex_time_limit`, `ex_questlimit_display`, `ex_description`, `ex_created`) VALUES
(42, 68, 'Pre-Test', '10', 2, 'sdadasd', '2021-10-26 11:10:59'),
(43, 68, 'Pre-Test4', '10', 2, 'ASasAS', '2021-10-27 12:12:55'),
(44, 67, 'Pre-Test5', '10', 2, 'qweqweqwewq', '2021-10-27 12:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks_tbl`
--

CREATE TABLE `feedbacks_tbl` (
  `fb_id` int(11) NOT NULL,
  `exmne_id` int(11) NOT NULL,
  `fb_exmne_as` varchar(1000) NOT NULL,
  `fb_feedbacks` varchar(1000) NOT NULL,
  `fb_date` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks_tbl`
--

INSERT INTO `feedbacks_tbl` (`fb_id`, `exmne_id`, `fb_exmne_as`, `fb_feedbacks`, `fb_date`) VALUES
(12, 94, 'Anonymous', 'asdada wdwaea', 'October 21, 2021');

-- --------------------------------------------------------

--
-- Table structure for table `question_category`
--

CREATE TABLE `question_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `category_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `question_category`
--

INSERT INTO `question_category` (`category_id`, `category_name`, `category_created`) VALUES
(1, 'English', '2021-10-06 07:50:20'),
(2, 'Filipino', '2021-10-06 07:50:20'),
(3, 'Math', '2021-10-06 07:22:22'),
(4, 'Science', '2021-10-06 07:53:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_acc`
--
ALTER TABLE `admin_acc`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `course_tbl`
--
ALTER TABLE `course_tbl`
  ADD PRIMARY KEY (`cou_id`);

--
-- Indexes for table `examinee_tbl`
--
ALTER TABLE `examinee_tbl`
  ADD PRIMARY KEY (`exmne_id`);

--
-- Indexes for table `exam_answers`
--
ALTER TABLE `exam_answers`
  ADD PRIMARY KEY (`exans_id`);

--
-- Indexes for table `exam_attempt`
--
ALTER TABLE `exam_attempt`
  ADD PRIMARY KEY (`examat_id`);

--
-- Indexes for table `exam_question_tbl`
--
ALTER TABLE `exam_question_tbl`
  ADD PRIMARY KEY (`eqt_id`);

--
-- Indexes for table `exam_tbl`
--
ALTER TABLE `exam_tbl`
  ADD PRIMARY KEY (`ex_id`);

--
-- Indexes for table `feedbacks_tbl`
--
ALTER TABLE `feedbacks_tbl`
  ADD PRIMARY KEY (`fb_id`);

--
-- Indexes for table `question_category`
--
ALTER TABLE `question_category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `English` (`category_id`,`category_name`,`category_created`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_acc`
--
ALTER TABLE `admin_acc`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course_tbl`
--
ALTER TABLE `course_tbl`
  MODIFY `cou_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `examinee_tbl`
--
ALTER TABLE `examinee_tbl`
  MODIFY `exmne_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `exam_answers`
--
ALTER TABLE `exam_answers`
  MODIFY `exans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433;

--
-- AUTO_INCREMENT for table `exam_attempt`
--
ALTER TABLE `exam_attempt`
  MODIFY `examat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `exam_question_tbl`
--
ALTER TABLE `exam_question_tbl`
  MODIFY `eqt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `exam_tbl`
--
ALTER TABLE `exam_tbl`
  MODIFY `ex_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `feedbacks_tbl`
--
ALTER TABLE `feedbacks_tbl`
  MODIFY `fb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `question_category`
--
ALTER TABLE `question_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

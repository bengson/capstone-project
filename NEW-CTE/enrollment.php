<?php include 'conn.php';?>
<!DOCTYPE html>
<html lang="en">

<style>
    html {
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }
   </style>
<head>
	<title>CTE- Reviewer Center</title>
	<meta charset="UTF-8">
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="login-ui/image/png" href="adminpanel/admin/login-ui/images/logo1.png"/>
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/util.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/main.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<script src="js/script.js" defer></script>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url(login-ui/images/bg1.jpg);  background-position: center; background-size: cover;  ">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(login-ui/images/index.png);">
					<span class="login100-form-title-1">
						Enrollment 
					</span>
				</div>


   <!--enrollment starts-->
   <form class="form"  method="POST"  enctype="multipart/form-data">

      
      <!-- Progress bar -->
      <div class="progressbar">
        <div class="progress" id="progress"></div>
        
        <div
          class="progress-step progress-step-active"
          data-title="Personal "
        ></div>
        <div class="progress-step" data-title="Contact"></div>
        <div class="progress-step" data-title="School"></div>
        <div class="progress-step" data-title="Payment"></div>
        <div class="progress-step" data-title="Password"></div>
      </div>

      <!-- Steps -->
      <div class="form-step form-step-active">

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="username">Full Name</label>
          <input type="text" name="username" id="username" class="form-control" required/>
        </div>
        <div class="form-group col-md-6">
          <label for="mname">Middle Name</label>
          <input type="text"  class="form-control" name="mname" id="mname"   required/>
        </div>
        </div>
        <div class="form-row">
        <div class="form-group col-md-6">
            <label>Birhdate</label>
            <input type="date" name="bdate" id="bdate" class="form-control" required />
          </div>
        <div class="form-group col-md-6">
    <label for="gender">Gender</label>
    <select class="form-control" name="gender" id="gender" required> 
    <option  value="" selected>Choose...</option>
      <option value="M">Male</option>
      <option value="F">Female</option>  
            </select>
  </div>
  </div>
        <div class="btns-group">
          <a href="index.php" class="btn btn-prev ">Back</a>
          <a href="#" class="btn btn-next ">Next</a>
        </div>
      </div>
      <div class="form-step">
        <div class="form-group">
          <label for="phno">Contact Number (Globe or TM Only)</label>
          <input type="text" class="form-control" name="phno" id="phno" required/>
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="text"  class="form-control" name="email" id="email"  required/>
        </div>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>

      <div class="form-step">
      <div class="form-row">
    <div class="form-group col-md-6">
    <label for="degree">Degree</label>
    <select class="form-control" name="degree" id="degree" required>
    <option  value="0" selected>Choose...</option>
    <?php 
                $selCourse = $conn->query("SELECT * FROM course_tbl ORDER BY cou_id asc");
                while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) { ?>
                  <option value="<?php echo $selCourseRow['cou_id']; ?>"><?php echo $selCourseRow['cou_name']; ?></option>
                <?php }
               ?>
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="school_grad">School Graduate</label>
    <input type="text" class="form-control" id="school_grad" name="school_grad" required>
  </div>
   </div>
   <div class="form-row">
  <div class="form-group col-md-6">
    <label for="year_grad">Year Graduate</label>
    <input type="text" class="form-control" id="year_grad" name="year_grad" required >
  </div>
  <div class="form-group col-md-6">
    <label for="award">Academic Awards</label>
    <select class="form-control" name="award" id="award" required>
    <option  value="" selected>Choose...</option>
      <option value="Cumlaude">Cumlaude</option>
      <option value="Magna Cumlaude">Magna Cumlaude</option>
      <option value="Summa Cumlaude">Summa Cumlaude</option>
	  <option value="None">None</option>
    </select>
  </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-6">
    <label for="take">First Taker</label>
    <select class="form-control" name="take" id="take" required>
    <option   value="" selected>Choose...</option>
      <option  value="Yes">Yes</option>
      <option  value="No">No</option>
    
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="majorship">Majorship</label>
    <select class="form-control" name="majorship" id="majorship" required>
    <option   value="" selected>Choose...</option>
      <option value="English">English</option>
      <option value="Filipino">Filipino</option>
      <option value="Mathemathics">Mathemathics</option>
	  <option value="Social Science">Social Science</option>
	  <option value="Biological Science">Biological Science</option>
	  <option value="Physical Science">Physical Science</option>
	  <option value="MAPEH">MAPEH</option>
	  <option value="TLE">TLE</option>
	  <option value="Fishery Arts">Fishery Arts</option>
    </select>
  </div>
  </div>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      <div class="form-step">
      <label><b> Mode of Payment:</b><p> 1. Personal Transaction at LNU Cashier's Office - Present QR Code at LNU Gate
    (Note: Go first to LNU CTE Office for the Registration Form to be presented at LNU Cashier)</p>

<p>2. Palawan Express Padala with details below:
          Name: Ruby Mae H. Peñaranda
          Mobile Num: 0967-253-0502
          Purpose of Transaction: LET Review Registration Fee</p>

<p>3. Gcash 
     (Note: Add 2% of the Total Amount of Payment for the Cash Out Service Fee)
     Send To: 09672530502
     Amount: Payment  + 2% Service Fee = Total Amount
     Message: 
        Name of Reviewee: _________________</p>

<p>Send picture of payment receipt to ctereview2021@gmail.com for confirmation.</p></label>

<div class="col-md-4 mb-3">
    <label for="pay">Payment Method</label>
    <select class="form-control" name="pay" id="pay" required>
	<option  value="" selected>Choose...</option>
      <option value="Personal Transaction at LNU Cashier's Office"> Personal Transaction at LNU Cashier's Office</option>
      <option value="Palawan">Palawan</option>
	  <option value="Gcash">Gcash</option>
     
      
    </select>
	</div>
	<div class="form-group">
    <label for="image">Picture of Signature Electronic signature (E-sign)</label>
    <input type="file" class="form-control-file" name="image" id="image" required>
  </div>
  <div class="col-md-6 mb-3">
      <label for="fb_name">Facebook Profile Name: (for Groupchat)</label>
      <input type="text" class="form-control" name="fb_name" id="fb_name" placeholder="Facebook Profile Name" required>
     
    </div>
   
  

      </label>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      
      <div class="form-step">
        <div class="form-group">
          <label for="password">Password</label>
          <input class="form-control" type="password" name="password" id="password" required />
</div>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <input type="submit" value="Submit"  name ="submit" id="submit"class="btn" />
        </div>
      </div>
      
    </form>
<!--enrollment end-->
<script>
  $('#submit').on('submit',function(){

    Swal.fire(
      'Good Job',
      'Click',
      'success'
    )

  })
</script>

<?php
include 'connection.php';

if(isset($_POST['submit'])){


  $username      = $_POST['username'];
  $mname         = $_POST['mname'];
  $bdate         = $_POST['bdate'];
  $gender        = $_POST['gender'];
  $phno          = $_POST['phno'];
  $email         = $_POST['email'];
  $degree        = $_POST['degree'];
  $school_grad   = $_POST['school_grad'];
  $year_grad     = $_POST['year_grad'];
  $award         = $_POST['award'];
  $take          = $_POST['take'];
  $majorship     = $_POST['majorship'];
  $pay           = $_POST['pay'];
  $fb_name       = $_POST['fb_name'];
  $password      = $_POST['password'];
  $image         = $_FILES['image']['name'];
 

    $select = "SELECT * FROM examinee_tbl WHERE exmne_email = '$email'";
    $result = mysqli_query($conn, $select);

  
    if(mysqli_num_rows($result) > 0){
      echo "<script>
           
      Swal.fire({
           icon: 'error',
           title: 'Ooops...',
           text: 'Email is already exist. Try again!',
           showConfirmButton: true,
           confirmButtonText:'Okay ',
           confirmButtonColor:'#1E90FF ',
           closeOnConfirm: false

           })

       </script>";
    }else{
        $register = "INSERT INTO examinee_tbl (exmne_fullname, exmne_mname , exmne_birthdate, exmne_gender, exmne_contact, exmne_email, exmne_course, exmne_schlgrad,  exmne_yrgrad, exmne_acad, exmne_take, exmne_majorship, exmne_payment, exmne_pic, exmne_fb, exmne_password, status) VALUES ('$username', '$mname' , '$bdate' , '$gender', '$phno' , '$email ', '$degree', '$school_grad' , '$year_grad' , '$award' , '$take' ,'$majorship', '$pay' ,'$image','$fb_name', '$password', 'pending')";
        mysqli_query($conn, $register);
       
        move_uploaded_file($_FILES["image"]["tmp_name"], "payment/".$_FILES["image"]["name"]);

        echo "<script>
           
                Swal.fire({
                     icon: 'success',
                     title: 'Submited Successful',
                     text: 'Please wait for the admin approval. Thank You!',
                     showConfirmButton: true,
                     confirmButtonText:'Proceed ',
                     confirmButtonColor:'#1E90FF ',
                     closeOnConfirm: false

                     })
                     .then(function() {
                      window.location = 'home.php';
                  });

                 </script>";
       
    }
}
?>



   </div>
		</div>
	</div>
	


	<script src="login-ui/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="login-ui/vendor/animsition/js/animsition.min.js"></script>
	<script src="login-ui/vendor/bootstrap/js/popper.js"></script>
	<script src="login-ui/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="login-ui/vendor/select2/select2.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/moment.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="login-ui/vendor/countdowntime/countdowntime.js"></script>
	<script src="login-ui/js/main.js"></script>





</body>
</html>

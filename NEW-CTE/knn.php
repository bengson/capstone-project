<?php
require_once __DIR__ . '/vendor/autoload.php';

use Phpml\Classification\KNearestNeighbors;

// raw_percentage = 100
// new_percentage = (raw_percentage / 10) - 1

$dataset = [
	[0,9,9,9],
	[0,9,9,8],
	[0,9,8,7],
	[0,8,7,6],
	[0,7,6,5],
	[0,6,5,4],
	[0,5,4,3],
	[0,4,3,2],
	[0,3,2,1],
	[0,2,1,1],
	[0,1,1,1],

	[9,0,9,9],
	[9,0,9,8],
	[9,0,8,7],
	[8,0,7,6],
	[7,0,6,5],
	[6,0,5,4],
	[5,0,4,3],
	[4,0,3,2],
	[3,0,2,1],
	[2,0,1,1],
	[1,0,1,1],

	[9,9,0,9],
	[9,9,0,8],
	[9,8,0,7],
	[8,7,0,6],
	[7,6,0,5],
	[6,5,0,4],
	[5,4,0,3],
	[4,3,0,2],
	[3,2,0,1],
	[2,1,0,1],
	[1,1,0,1],

	[9,9,9,0],
	[9,9,8,0],
	[9,8,7,0],
	[8,7,6,0],
	[7,6,5,0],
	[6,5,4,0],
	[5,4,3,0],
	[4,3,2,0],
	[3,2,1,0],
	[2,1,1,0],
	[1,1,1,0]
];

$labels = [
	'Math',
	'Math',
	'Math',
	'Math',
	'Math',
	'Math',
	'Math',
	'Math',
	'Math',
	'Math',
	'Math',

	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',
	'Filipino',

	'English',
	'English',
	'English',
	'English',
	'English',
	'English',
	'English',
	'English',
	'English',
	'English',
	'English',

	'Science',
	'Science',
	'Science',
	'Science',
	'Science',
	'Science',
	'Science',
	'Science',
	'Science',
	'Science',
	'Science'
];

$classifier = new KNearestNeighbors();
$classifier->train($dataset, $labels);

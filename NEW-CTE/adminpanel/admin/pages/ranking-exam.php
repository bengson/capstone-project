
<style>
td {
  text-align: left;
}

.table-responsive thead th {
    
    color: #000;
    text-align: center;
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }   
}
  </style>
<div class="app-main__outer">
        <div class="app-main__inner">
             


            <?php 
                @$exam_id = $_GET['exam_id'];


                if($exam_id != "")
                {
                   $selEx = $conn->query("SELECT * FROM exam_tbl WHERE ex_id='$exam_id' ")->fetch(PDO::FETCH_ASSOC);
                   $exam_course = $selEx['cou_id'];

                   

                   $selExmne = $conn->query("SELECT * FROM examinee_tbl et  WHERE exmne_course='$exam_course'  ");


                   ?>
                   <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div><b class="text-primary">RESULTS</b><br>
                                Exam Name : <?php echo $selEx['ex_title']; ?><br><br>
                            </div>
                        </div>
                    </div>
                    </div>
                    

        <div class="text-right">
      <input type="button" class="btn btn-warning" id="btnExport" value="REPORTS" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#btnExport", function () {
            html2canvas($('#tblCustomers')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Pre-test_Report.pdf");
                }
            });
        });
    </script>
    </div>
               

                    <div class="table-responsive"  id="tblCustomers" cellspacing="0" cellpadding="0">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                        
                         
                            <thead >
                                <tr>
                                    <th class="text-center pl-4" width="25%">Examinee Fullname</th>
                                    <th class="text-center pl-4 ">Score / Over</th>
                                    <th class="text-center pl-4">Percentage</th>
                                    <th class="text-center pl-4">Cluster</th>
                                    <th class="text-center pl-4">Rank</th>
                                    <th class="text-center pl-4">Date</th>
                                </tr>
                            </thead>
                            <?php 
                            
                                while ($selExmneRow = $selExmne->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <?php 
                                            $exmneId = $selExmneRow['exmne_id'];
                                            $selScore = $conn->query("SELECT * FROM exam_question_tbl eqt INNER JOIN exam_answers ea ON eqt.eqt_id = ea.quest_id AND eqt.exam_answer = ea.exans_answer  WHERE ea.axmne_id='$exmneId' AND ea.exam_id='$exam_id' AND ea.exans_status='new' ORDER BY ea.exans_id DESC");

                                              $selAttempt = $conn->query("SELECT * FROM exam_attempt WHERE exmne_id='$exmneId' AND exam_id='$exam_id' ");

                                             $over = $selEx['ex_questlimit_display']  ;    


                                              @$score = $selScore->rowCount();
                                                @$ans = $score / $over * 100;

                                         ?>
                                        
                                        <td class="text-center pl-4">

                                          <?php echo $selExmneRow['exmne_fullname']; ?></td>
                                        
                                        <td class="text-center pl-4" >
                                        <?php 
                                          if($selAttempt->rowCount() == 0)
                                          {
                                            echo "Not answer yet";
                                          }
                                          else if($selScore->rowCount() > 0)
                                          {
                                            echo $totScore =  $selScore->rowCount();
                                            echo " / ";
                                            echo $over;
                                          }
                                          else
                                          {
                                            echo $totScore =  $selScore->rowCount();
                                            echo " / ";
                                            echo $over;
                                          }

                                         ?>
                                        </td>
                                        
                                        <td class="text-center pl-4">
                                          <?php 
                                                if($selAttempt->rowCount() == 0)
                                                {
                                                  echo "Not answer yet";
                                                }
                                                else
                                                {
                                                    echo number_format($ans,2); ?>%<?php
                                                }
                                           
                                          ?>
                                        </td>

                                                
                                        <td class="text-center pl-4">
                                                  
                                                  <?php
                                          //$ans = $score;                                   
      
                                          if ($ans >= 1 && $ans < 33){
                                              echo 'C';
                                          }
                                          elseif ($ans >= 33 && $ans < 66){
                                              echo 'B';
      
                                          }
                                          elseif ($ans >= 66 && $ans <= 100){
                                              echo 'A';
                                          }
                                          elseif ($ans >= 0 && $ans <= 0){
                                            echo 'N/A';
                                        }
    
      
                                          ?>
                                         
                                              </td>
                                              <td class="text-center pl-4">
                                                  
                                                  <?php
                                          //$ans = $score;                                   
      
                                          if ($ans >= 1 && $ans <= 10){
                                              echo '10';
                                          }
                                          elseif ($ans >= 11 && $ans <= 20){          
                                              echo '9';
                                          }
                                          elseif ($ans >=21 && $ans <= 30){
                                              echo '8';
                                          }
                                          elseif ($ans >=31 && $ans <= 40){
                                            echo '7';
                                        }
                                          elseif ($ans >=41 && $ans <= 50){
                                            echo '6';
                                        }
                                        elseif ($ans >=51 && $ans <= 60){
                                            echo '5';
                                        }
                                        elseif ($ans >=61 && $ans <= 70){
                                            echo '4';
                                        }
                                        elseif ($ans >=71 && $ans <= 80){
                                            echo '3';
                                        }
                                        elseif ($ans >=81 && $ans <= 90){
                                            echo '2';
                                        }
                                        elseif ($ans >=91 && $ans <= 100){
                                            echo '1';
                                        }
                                        elseif ($ans == 0){
                                            echo 'N/A';
                                        }
    
      
                                          ?>
                                          
                                         
                                              </td>
                                              <td  class="text-center pl-4">
                                            <?php 
                                                 $eid = $selExmneRow['exmne_id'];
                                                 $selExName = $conn->query("SELECT * FROM exam_tbl et INNER JOIN exam_answers ea ON et.ex_id=ea.exam_id WHERE  ea.axmne_id='$eid' ")->fetch(PDO::FETCH_ASSOC);
                                                 echo $selExName['exans_created']??= 'Not yet answering';
                                               ?>   
                                            </td>

                                    </tr>
                                <?php }
                             ?>                              
                          </tbody>
                        </table>
                    </div>



                   <?php
                }
                else
                { ?>
                <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div><b>EXAM</b></div>
                    </div>
                </div>
                </div> 

                 <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">ExAM List
                    </div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                            <thead>
                            <tr>
                                <th class="text-left pl-4">Exam Title</th>
                                <th class="text-left">Course</th>
                                <th class="text-center" width="10%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $selExam = $conn->query("SELECT * FROM exam_tbl ORDER BY ex_id DESC ");
                                if($selExam->rowCount() > 0)
                                {
                                    while ($selExamRow = $selExam->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <tr>
                                            <td class="pl-4"><?php echo $selExamRow['ex_title']; ?></td>
                                            <td>
                                                <?php 
                                                    $courseId =  $selExamRow['cou_id']; 
                                                    $selCourse = $conn->query("SELECT * FROM course_tbl WHERE cou_id='$courseId' ");
                                                    while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) {
                                                        echo $selCourseRow['cou_name'];
                                                    }
                                                ?>
                                            </td>
                                            <td class="text-center">
                                             <a href="?page=ranking-exam&exam_id=<?php echo $selExamRow['ex_id']; ?>"  class="btn btn-outline-warning btn-sm"><i class="fas fa-eye"></i>&nbsp&nbspView</a>
                                            </td>
                                        </tr>

                                    <?php }
                                }
                                else
                                { ?>
                                    <tr>
                                      <td colspan="5">
                                        <h3 class="p-3">No Exam Found</h3>
                                      </td>
                                    </tr>
                                <?php }
                               ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>   
                    
                <?php }

             ?>         
        
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.css"/> 

<script>
$(document).ready(function () {
    $.noConflict();
    var table = $('#tableList').DataTable();
});</script>

         



















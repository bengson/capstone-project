<style>
td {
  text-align: center;
}
.table-responsive thead th {
    
    color: #000;
  
 
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }   
}
</style>
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>MANAGE EXAM</div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">Exam List
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                        <thead>
                            <tr>
                                <th class="text-center pl-4">Exam Title</th>
                                <th class="text-center ">Course</th>
                                <th class="text-center ">Description</th>
                                <th class="text-center ">Time limit</th>
                                <th class="text-center ">Display limit</th>
                                <th class="text-center" width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $selExam = $conn->query("SELECT * FROM exam_tbl ORDER BY ex_id DESC ");
                            if ($selExam->rowCount() > 0) {
                                while ($selExamRow = $selExam->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <tr>
                                        <td class="pl-4"><?php echo $selExamRow['ex_title']; ?></td>
                                        <td>
                                            <?php
                                            $courseId =  $selExamRow['cou_id'];
                                            $selCourse = $conn->query("SELECT * FROM course_tbl WHERE cou_id='$courseId' ");
                                            while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) {
                                                echo $selCourseRow['cou_name'];
                                            }
                                            ?>

                                        </td>

                                        <td><?php echo $selExamRow['ex_description']; ?></td>
                                        <td><?php echo $selExamRow['ex_time_limit']; ?></td>
                                        <td><?php echo $selExamRow['ex_questlimit_display']; ?></td>
                                        <td class="text-center">
                                            <button onclick="location.href='manage-exam.php?id=<?php echo $selExamRow['ex_id']; ?>';" type="button" class="btn btn-outline-primary btn-sm"><i class="fas fa-pencil-alt"></i>&nbsp&nbspManage</button>
                                            <button type="button" id="deleteExam" data-id='<?php echo $selExamRow['ex_id']; ?>' class="btn btn-outline-danger btn-sm"><i class="fas fa-trash-alt"></i>&nbsp&nbspDelete</button>
                                        </td>
                                    </tr>

                                <?php }
                            } else { ?>
                                <tr>
                                    <td colspan="5">
                                        <h3 class="p-3">No Exam Found</h3>
                                    </td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.css"/> 

<script>
$(document).ready(function () {
    $.noConflict();
    var table = $('#tableList').DataTable();
});</script>


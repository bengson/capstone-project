

<div class="app-main__outer">
    <div id="refreshData">
    <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                       
                        <div>

                        <img src="assets/images/dash1.jpg" class="img-fluid" alt="Responsive image" height="100px" width="1180" style=" border-radius: 15px;" >
                           
                        </div>
                    </div>
                    <div class="page-title-actions">
                       
                       
                        </div>
                    </div>   
                 </div>
             <div class="row">
                <div class="col-md-4 col-xl-3">
                    <div class="card mb-3 widget-content  " style="background-color:#5DA5DA;">
                        <div class="widget-content-wrapper text-white">
                            <div class="widget-content-left">
                                <div class="widget-heading"><i class="fas fa-tasks"  style="font-size:50px;"></i>&nbsp &nbspTotal Exam</div>
                                <div class="widget-subheading" style="color:transparent;">.</div>
                                <a  href="home.php?page=manage-exam" style="color:white; font-size:17px;"> More Info <i class="fas fa-arrow-right" style="font-size:17px;"></i></a>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-white">
                                    <span><?php echo $totalCourse = $selExam['totExam']; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-3">
                    <div class="card mb-3 widget-content " style="background-color: #5DA5DA;">
                        <div class="widget-content-wrapper text-white">
                            <div class="widget-content-left">
                                <div class="widget-heading"><i class="fas fa-users"  style="font-size:50px;"></i>&nbsp&nbsp Examinee</div>
                                <div class="widget-subheading" style="color:transparent;">.</div>
                                <a  href="home.php?page=manage-examinee" style="color:white; font-size:17px;"> More Info <i class="fas fa-arrow-right" style="font-size:17px;"></i></a>
                            </div>
                            <div class="widget-content-right">
                            <div class="widget-numbers text-white">
                                    <span><?php echo $selExaminee = $selExaminee['totExaminee']; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-xl-3">
                    <div class="card mb-3 widget-content " style="background-color: #5DA5DA;">
                        <div class="widget-content-wrapper text-white">
                            <div class="widget-content-left">
                                <div class="widget-heading"><i class="fas fa-user-check"  style="font-size:50px;"></i>&nbsp &nbspTotal Approval</div>
                                <div class="widget-subheading" style="color:transparent;">.</div>
                                <a  href="home.php?page=manage-approval" style="color:white; font-size:17px;"> More Info <i class="fas fa-arrow-right" style="font-size:17px;"></i></a>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-white">
                                    <span><?php echo $selapproval = $selapproval['totapproval']; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-3">
                    <div class="card mb-3 widget-content "style="background-color: #5DA5DA;">
                        <div class="widget-content-wrapper text-white">
                            <div class="widget-content-left">
                                <div class="widget-heading"><i class="fas fa-comment-alt"  style="font-size:50px;"> </i>&nbsp &nbspTotal Feedback</div>
                                <div class="widget-subheading" style="color:transparent;">.</div>
                                <a  href="home.php?page=feedbacks" style="color:white; font-size:17px;"> More Info <i class="fas fa-arrow-right" style="font-size:17px;"></i></a>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-white">
                                    <span><?php echo $selfeedback = $selfeedback['totfeedback']; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           



        </div>
         
    </div>

<div class="app-main__outer">
        <div class="app-main__inner">
             


            <?php 
                @$exmne_id = $_GET['exmne_id'];


                if($exmne_id != "")
                {
                   $selEx = $conn->query("SELECT * FROM examinee_tbl WHERE exmne_id='$exmne_id' ")->fetch(PDO::FETCH_ASSOC);
                   $exam_course = $selEx['exmne_id'];

                   

                   $selExmne = $conn->query("SELECT * FROM examinee_tbl et  WHERE exmne_id='$exmne_id' ");


                   ?>
                   <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div><b class="text-primary">HISTORY LIST</b><br>
                               
                            </div>
                        </div>
                    </div>
                    </div>


                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                          <tbody>
                            <thead>
                                <tr>
                                    <th width="25%">Examinee Fullname</th>
                                    <th>Score / Over</th>
                                    <th>Percentage</th>
                                    <th>Date Exam</th>
                                </tr>
                            </thead>
                            <?php 
                             
                                while ($selExmneRow = $selExmne->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <?php 
                                            $exmneId = $selExmneRow['exmne_id'];
                                            $selScore = $conn->query("SELECT * FROM exam_question_tbl eqt INNER JOIN exam_answers ea ON eqt.eqt_id = ea.quest_id AND eqt.exam_answer = ea.exans_answer  WHERE ea.axmne_id='$exmneId'  AND ea.exans_status='new' ORDER BY ea.exans_id DESC");

                                            //$selAttempt = $conn->query("SELECT * FROM exam_attempt WHERE exmne_id='$exmneId' AND exam_id='$exam_id' ");
                                              $selAttempt = $conn->query("SELECT SUM(category_score) AS exam_score, exmne_fullname,ex_title FROM `score_per_category_with_answer_checking` WHERE exmne_id='$exmneId' GROUP BY ex_id,exmne_id; ");
                                              
                                              
                                             $over = $selEx['ex_questlimit_display']  ;    


                                              @$score = $selScore->rowCount();
                                                @$ans = $score + $over * 100;

                                         ?>
                                     
                                        <td class="text-center">

                                          <?php echo $selExmneRow['exmne_fullname']; ?>
                                          
                                          </td>
                                        
                                        <td class="text-center">
                                        <?php 
                                          if($selAttempt->rowCount() == 0)
                                          {
                                            echo "Not answer yet";
                                          }
                                          else if($selScore->rowCount() > 0)
                                          {
                                            echo $totScore =  $selScore->rowCount();
                                            echo " / ";
                                            echo $over;
                                          }
                                          else
                                          {
                                            echo $totScore =  $selScore->rowCount();
                                            echo " / ";
                                            echo $over;
                                          }

                                            
                                            

                                         ?>
                                        </td>
                                        <td class="text-center">
                                          <?php 
                                                if($selAttempt->rowCount() == 0)
                                                {
                                                  echo "Not answer yet";
                                                }
                                                else
                                                {
                                                    echo number_format($ans,2); ?>%<?php
                                                }
                                           
                                          ?>
                                        </td>

                                        <td  class="text-center pl-4">
                                            
                                            <?php 
                                                 $eid = $selExmneRow['exmne_id'];
                                                 $selExName = $conn->query("SELECT * FROM exam_tbl et INNER JOIN exam_answers ea ON et.ex_id=ea.exam_id WHERE  ea.axmne_id='$eid' ")->fetch(PDO::FETCH_ASSOC);
                                                 echo $selExName['exans_created']??= 'Not yet answering';
                                               ?>
                                           
                                         
                                       
                                                  
                                            </td>

                                    </tr>
                                <?php }
                             ?>                              
                          </tbody>
                        </table>
                    </div>





                   <?php
                }
                else
                { ?>
                <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div><b>HISTORY LIST</b></div>
                    </div>
                </div>
                </div> 

                 <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Examinee List
                    </div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                            <thead>
                            <tr>
                                <th class="text-left pl-4">Examinee </th>
                                
                                <th class="text-center" width="10%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $selExam = $conn->query("SELECT * FROM examinee_tbl ORDER BY exmne_id DESC ");
                                if($selExam->rowCount() > 0)
                                {
                                    while ($selExamRow = $selExam->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <tr>
                                            <td class="pl-4"><?php echo $selExamRow['exmne_fullname']; ?></td>
                                            
                                            
                                            <td class="pl-4">
                                             <a href="?page=manage-history&exmne_id=<?php echo $selExamRow['exmne_id']; ?>"  class="btn btn-outline-warning btn-sm"><i class="fas fa-eye"></i>&nbsp&nbspView History</a>
                                            </td>
                                        </tr>

                                    <?php }
                                }
                                else
                                { ?>
                                    <tr>
                                      <td colspan="5">
                                        <h3 class="p-3">No Exam Found</h3>
                                      </td>
                                    </tr>
                                <?php }
                               ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>   
                    
                <?php }

             ?>      
            
            
      
        
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.css"/> 

<script>
$(document).ready(function () {
    $.noConflict();
    var table = $('#tableList').DataTable();
});</script>



















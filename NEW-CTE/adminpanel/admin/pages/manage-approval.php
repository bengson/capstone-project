<?php include ('connection.php'); ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<link href="css/sweetalert.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<?php



if(isset($_POST['approve'])){
    $id = $_POST['exmne_id'];
    
    $select = "UPDATE examinee_tbl SET status = 'approved' WHERE exmne_id = '$id'";
    $result = mysqli_query($conn, $select);

    echo "<script>
           
    Swal.fire({
         icon: 'success',
         title: 'Student Approve',
         text: 'Student Successfully Enrolled!',
         showConfirmButton: true,
         confirmButtonText:'Confirm ',
         confirmButtonColor:'#1E90FF ',
         closeOnConfirm: false

        
      });

     </script>";

}

if(isset($_POST['deny'])){
    $id = $_POST['exmne_id'];

    $select = "DELETE FROM examinee_tbl WHERE exmne_id = '$id'";
    $result = mysqli_query($conn, $select);

    echo "<script>
           
    Swal.fire({
         icon: 'success',
         title: 'Student Deny',
         text: 'Student Successfully Deny!',
         showConfirmButton: true,
         confirmButtonText:'Okay ',
         confirmButtonColor:'#1E90FF ',
         closeOnConfirm: false

         })

     </script>";

}

?>


 <style>
#main{width:100%; height:100%; position: absolute;}   
#my_image:hover{opacity: 0.7;}
#appear_image_div{width:100%; height:100%; position: fixed; bottom: 0; right: 0; z-index: 10; opacity: 0.7; background:#000000; top: 0px;}
#appear_image{ display:block; margin:auto; left:350px; width:60%; height:auto; position: fixed; bottom: 0;  z-index: 11; top:30px}
 </style>
 <style>
 th {
  text-align: center;
}
.table-responsive thead tr {
    
    color: #000;
    text-align: left;
 
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }   
}
</style>
<script>
$(document).ready(function(){
    setInterval(function(){
        $("#autodata").load("manage-approval.php");
        refresh();
    },10000);

});
</script>

<div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>MANAGE APPROVAL</div>
                    </div>
                </div>
            </div>        
            
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Examinee List Approval
                    </div>
                    <div class="table-responsive" id="autodata">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
                            <thead>
                            <tr>
                           
                             <th>Username</th>
                             <th>Contact Number</th>
                             <th>Facebook Acc.</th>
                             <th>Payment Proof</th>
                             <th>Email Address</th>

                             <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                           
                            
 
        <?php
            $query = "SELECT * FROM examinee_tbl WHERE status = 'pending' ORDER BY exmne_id ASC";
            $result = mysqli_query($conn, $query);
            while($row = mysqli_fetch_array($result)){
        ?>
             <tr>
           
            <td><?php echo $row['exmne_fullname'];?></td>
            <td><?php echo $row['exmne_contact'];?></td>
            <td><?php echo $row['exmne_fb'];?></td>
            <td>   
                <img  class="my_image" src="<?php echo "http://localhost/NEW-CTE/payment/".$row['exmne_pic']; ?>"  width="150px" height="150px" alt="Image">
                
            </td>
            <td><?php echo $row['exmne_email'];?></td>
            <td>
                <form  method ="POST">
                    <input type = "hidden" name  ="exmne_id" value = "<?php echo $row['exmne_id'];?>"/>
                    <input type = "submit" name  ="approve" class="btn btn-lg btn-outline-primary" value = "Approve"/>
                    <input type = "submit" name  ="deny" class="btn btn-lg btn-outline-danger" value = "Deny"/>
                </form>
            </td>
            </tr>
            
            <?php
            }
            ?>
                            </tbody>
                        </table>
                   


                    </div>
                </div>
            </div>
      
        
</div>
         

<script type="text/javascript">
    jQuery(function($){
        $('.my_image') .click(function(){
            var img= $(this) .attr("src");
            var appear_image = "<div id='appear_image_div' onclick='closeImage()'></div>";
            appear_image = appear_image.concat("<img id='appear_image' src='"+img+"'/>");

            $('body').append(appear_image);

        });
    });
function closeImage(){
    $('#appear_image_div').remove();
    $('#appear_image').remove();

}
</script>



<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.6.0/dt-1.11.3/fc-4.0.1/sc-2.0.5/sp-1.4.0/datatables.min.css"/> 

<script>
$(document).ready(function () {
    $.noConflict();
    var table = $('#tableList').DataTable();
    
});</script>







<!-- Modal For Add Course -->
<div class="modal fade" id="modalForAddCourse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
   <form class="refreshFrm" id="addCourseFrm" method="post">
     <div class="modal-content">
      <div class="modal-header" >
        <h5 class="modal-title" id="exampleModalLabel" style="color:black"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/course-assign.png"/>&nbsp&nbspAdd Course</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="form-group">
            <label>Course</label>
            <input type="" name="course_name" id="course_name" class="form-control" placeholder="Input Course" required="" autocomplete="off">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-outline-primary">Add Now</button>
      </div>
    </div>
   </form>
  </div>
</div>


<!-- Modal For Update Course -->
<div class="modal fade myModal" id="updateCourse-<?php echo $selCourseRow['cou_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
     <form class="refreshFrm" id="addCourseFrm" method="post" >
       <div class="modal-content myModal-content" >
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style="color:black"> <i class="fas fa-graduation-cap"Style="font-size:30px"></i>&nbsp&nbspUpdate ( <?php echo $selCourseRow['cou_name']; ?> )</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <div class="form-group">
              <label>Course</label>
              <input type="" name="course_name" id="course_name" class="form-control" value="<?php echo $selCourseRow['cou_name']; ?>">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline-primary">Update Now</button>
        </div>
      </div>
     </form>
    </div>
  </div>


<!-- Modal For Add Exam -->
<div class="modal fade" id="modalForExam" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
   <form class="refreshFrm" id="addExamFrm" method="post">
     <div class="modal-content">
      <div class="modal-header" >
        <h5 class="modal-title" id="exampleModalLabel" style="color:black"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/--add-document.png"/>&nbsp&nbspAdd Exam</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="form-group">
            <label>Select Course</label>
            <select class="form-control" name="courseSelected">
              <option value="0">Select Course</option>
              <?php 
                $selCourse = $conn->query("SELECT * FROM course_tbl ORDER BY cou_id DESC");
                if($selCourse->rowCount() > 0)
                {
                  while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) { ?>
                     <option value="<?php echo $selCourseRow['cou_id']; ?>"><?php echo $selCourseRow['cou_name']; ?></option>
                  <?php }
                }
                else
                { ?>
                  <option value="0">No Course Found</option>
                <?php }
               ?>
            </select>
          </div>

          <div class="form-group">
            <label>Exam Time Limit</label>
            <select class="form-control" name="timeLimit" required="">
              <option value="0">Select time</option>
              <option value="10">10 Minutes</option> 
              <option value="20">20 Minutes</option> 
              <option value="30">30 Minutes</option> 
              <option value="40">40 Minutes</option> 
              <option value="50">50 Minutes</option> 
              <option value="60">60 Minutes</option> 
            </select>
          </div>

          <div class="form-group">
            <label>Question Limit to Display</label>
            <input type="number" name="examQuestDipLimit" id="" class="form-control" placeholder="Input question limit to display">
          </div>

          <div class="form-group">
            <label>Exam Title</label>
            <input type="" name="examTitle" class="form-control" placeholder="Input Exam Title" required="">
          </div>

          <div class="form-group">
            <label>Exam Description</label>
            <textarea name="examDesc" class="form-control" rows="4" placeholder="Input Exam Description" required=""></textarea>
          </div>


        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-outline-primary">Add Now</button>
      </div>
    </div>
   </form>
  </div>
</div>


<!-- Modal For Add Examinee -->
<div class="modal fade " id="modalForAddExaminee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
   <form class="refreshFrm" id="addExamineeFrm" method="post">
     <div class="modal-content">
      <div class="modal-header" >
        <h5 class="modal-title" id="exampleModalLabel"  style="color:black"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/add-user-male.png"/>&nbsp&nbsp Add Examinee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" align="center">
        <div class="col-md-11 " >
          
      <label > <b> Personal Information </b></label>
          <div class="form-row" >
          <div class="form-group col-md-6">
            <label >Fullname</label>
            <input type="" name="fullname" id="fullname" class="form-control " autocomplete="off" required="">
          </div>
          
          <div class="form-group col-md-6 ">
            <label >Middle name</label>
            <input type="" name="mname" id="mname" class="form-control "  autocomplete="off" required="">
          </div>
          </div>
          <div class="form-row">
          <div class="form-group col-md-6">
            <label>Birhdate</label>
            <input type="date" name="bdate" id="bdate" class="form-control" placeholder="Input Birhdate" autocomplete="off" >
          </div>
          <div class="form-group col-md-6">
            <label>Gender</label>
            <select class="form-control" name="gender" id="gender">
            <option   value="" selected>Choose...</option>
              <option value="M">Male</option>
              <option value="F">Female</option>
            </select>
          </div>
          </div>
          <label > <b> Contact Information </b></label>
          <div class="form-row">
          <div class="form-group col-md-6">
            <label>Contact Number (Globe or TM)</label>
            <input type="" name="contact" id="contact" class="form-control"  autocomplete="off" required="">
          </div>
          <div class="form-group col-md-6">
            <label>Student ID</label>
            <input type="" name="student_id" id="student_id" class="form-control"  autocomplete="off" required="">
          </div>
          </div>
          <label > <b> School Information </b></label>
          <div class="form-row">
          <div class="form-group  col-md-6">
            <label>Degree</label>
            <select class="form-control" name="course" id="course">
            <option   value="" selected>Choose...</option>
              <?php 
                $selCourse = $conn->query("SELECT * FROM course_tbl ORDER BY cou_id asc");
                while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) { ?>
                  <option value="<?php echo $selCourseRow['cou_id']; ?>"><?php echo $selCourseRow['cou_name']; ?></option>
                <?php }
               ?>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>Student Graduate</label>
            <input type="" name="school_grad" id="school_grad" class="form-control"  autocomplete="off" required="">
          </div>
          <div class="form-group col-md-6">
            <label>Year Graduate</label>
            <input type="" name="year_grad" id="year_grad" class="form-control"  autocomplete="off" required="">
          </div>
          <div class="form-group col-md-6">
            <label>Academic Award</label>
            <select class="form-control" name="acad" id="acad">
            <option   value="" selected>Choose...</option>
              <option value="C">Cumlaude</option>
              <option value="MC"> Magna Cumlaude</option>
              <option value="SC"> Summa Cumlaude</option>
              <option value="N">None</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>First Taker</label>
            <select class="form-control" name="take" id="take">
            <option   value="" selected>Choose...</option>
      <option  value="Yes">Yes</option>
      <option  value="No">No</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>Majorship</label>
            <select class="form-control" name="majorship" id="majorship">
            <option   value="" selected>Choose...</option>
      <option value="English">English</option>
      <option value="Filipino">Filipino</option>
      <option value="Mathemathics">Mathemathics</option>
	  <option value="Social Science">Social Science</option>
	  <option value="Biological Science">Biological Science</option>
	  <option value="Physical Science">Physical Science</option>
	  <option value="MAPEH">MAPEH</option>
	  <option value="TLE">TLE</option>
	  <option value="Fishery Arts">Fishery Arts</option>
            </select>
          </div>
          </div>
          <label > <b>Account Information </b></label>
          <div class="form-row">
          <div class="form-group col-md-6">
            <label>Email</label>
            <input type="email" name="email" id="email" class="form-control"  autocomplete="off" required="">
          </div>
          
          <div class="form-group col-md-6">
            <label>Password</label>
            <input type="password" name="password" id="password" class="form-control"  autocomplete="off" required="">
          </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-outline-primary">Add Now</button>
      </div>
    </div>
   </form>
  </div>
</div>



<!-- Modal For Add Question -->
<div class="modal fade" id="modalForAddQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
   <form class="refreshFrm" id="addQuestionFrm" method="post">
     <div class="modal-content">
      <div class="modal-header">

        <h5 class="modal-title" id="exampleModalLabel">Add Question for <br><?php echo $selExamRow['ex_title']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="refreshFrm" method="post" id="addQuestionFrm">
      <div class="modal-body">
        <div class="col-md-12">
          <div class="form-group">
            <label>Questions Category</label>
            <select class="form-control" name="categoryId" value="<?php echo $category; ?>">
              <option value="0">Category</option>
              <?php 
                $selCourse = $conn->query("SELECT * FROM question_category ORDER BY category_id DESC");
                if($selCourse->rowCount() > 0)
               {
                  while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) { ?>
                     <option value="<?php echo $selCourseRow['category_id']; ?>"><?php echo $selCourseRow['category_name']; ?></option>
                  <?php }
                }
                
              ?>
            </select>
          </div>
 
      
      <div class="modal-body">
        <div class="col-md-12">
          <div class="form-group">
            <label>Question</label>
            <input type="hidden" name="examId" value="<?php echo $exId; ?>">
            <input type="" name="question" id="course_name" id="Category" class="form-control" placeholder="Input question" autocomplete="off">
          </div>

          <fieldset>
            <legend>Input word for choice's</legend>
            <div class="form-group">
                <label>Choice A</label>
                <input type="" name="choice_A" id="choice_A" class="form-control" placeholder="Input choice A" autocomplete="off">
            </div>

            <div class="form-group">
                <label>Choice B</label>
                <input type="" name="choice_B" id="choice_B" class="form-control" placeholder="Input choice B" autocomplete="off">
            </div>

            <div class="form-group">
                <label>Choice C</label>
                <input type="" name="choice_C" id="choice_C" class="form-control" placeholder="Input choice C" autocomplete="off">
            </div>

            <div class="form-group">
                <label>Choice D</label>
                <input type="" name="choice_D" id="choice_D" class="form-control" placeholder="Input choice D" autocomplete="off">
            </div>

            <div class="form-group">
                <label>Correct Answer</label>
                <input type="" name="correctAnswer" id="" class="form-control" placeholder="Input correct answer" autocomplete="off">
            </div>
          </fieldset>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Close</button>
        <button onclick="location.reload();" type="submit" class="btn btn-outline-primary">Add Now</button>
      </div>
      </form>
    </div>
   </form>
  </div>
</div>


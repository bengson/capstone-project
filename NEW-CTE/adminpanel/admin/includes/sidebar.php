<style>
 th {
  text-align: center;
}
.table-responsive thead tr {
    
    color: #000;
    text-align: left;
 
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }   
}
</style>
   <div class="app-sidebar sidebar-shadow" style="background-color:#e1ad01; overflow-y: scroll; ">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>   

                     <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <br>
                                

                            <div align="center">
                            <img src="assets/images/use.png" height="100" width="100" >
                           
                            </div>
                            <br>
                            <div align="center" style="color: black;"><label  style="color: black;" ><b> Review Master</b></label></div>
                            
                                <li class="app-sidebar__heading"><a href="home.php" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/speedometer.png"/>&nbsp&nbspDashboard</a></li>
                                

                                <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/courses.png"/>&nbsp &nbspMANAGE COURSE</li>
                                <li>
                                    <a href="#" style="color: black;">
                                        
                                         Course
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#modalForAddCourse" style="color: black;">
                                                <i class="metismenu-icon"></i>
                                                Add Course
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home.php?page=manage-course" style="color: black;">
                                                <i class="metismenu-icon">
                                                </i>Manage Course
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                               
                                <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/check-document.png"/>&nbsp &nbspMANAGE EXAM</li>
                                <li>
                                    <a href="#" style="color: black;">
                                        
                                         Exam
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#modalForExam" style="color: black;">
                                                <i class="metismenu-icon"></i>
                                                Add Exam
                                            </a>
                                        </li>
                                        <li>
                                            <a href="home.php?page=manage-exam" style="color: black;">
                                                <i class="metismenu-icon">
                                                </i>Manage Exam
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                           
                         
                                <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/edit-user.png"/>&nbsp &nbspMANAGE EXAMINEE</li>
                                <li>
                                <a href="#" style="color: black;">
                                         
                                         Examinee
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                    <a href="" data-toggle="modal" data-target="#modalForAddExaminee" style="color: black;">
                                        <i class="metismenu-icon pe-7s-add-user">
                                        </i>Add Examinee
                                    </a>
                                    </li>
                               
                                <li>
                                    <a href="home.php?page=manage-examinee" style="color: black;">
                                        <i class="metismenu-icon pe-7s-users">
                                        </i>Manage Examinee
                                    </a>
                                    </li>
                                    <li>
                                    <a href="home.php?page=manage-approval" style="color: black;">
                                        <i class="metismenu-icon pe-7s-users">
                                        </i>Manage Approval
                                    </a>
                                    </li>
                                    <li>
                                    <a href="home.php?page=manage-history" style="color: black;">
                                        <i class="metismenu-icon pe-7s-users">
                                        </i>Examinee History
                                    </a>
                                    </li>
                                    </ul>
                                </li>
                                
                                <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/leaderboard.png"/>&nbsp&nbspRESULTS</li>
                                <li>
                                <a href="#" style="color: black;">
                                         
                                         Examinee Results
                                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                <li>
                                    <a href="home.php?page=ranking-exam" style="color: black;">
                                       
                                        </i>View Result
                                    </a>
                                </li>
                                </ul>
                                </li>
                               
                               
                              

                                 <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/comments--v1.png"/>&nbsp&nbspFEEDBACKS</li>
                                <li>
                                    <a href="home.php?page=feedbacks" style="color: black;">
                                        
                                        </i>All Feedbacks
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>  
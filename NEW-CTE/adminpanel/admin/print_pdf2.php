<?php
	function generateRow(){
		$contents = '';
		include_once('../../conn.php');
		$sql = "SELECT * FROM examinee_tbl et INNER JOIN exam_attempt ea ON et.exmne_id = ea.exmne_id ORDER BY ea.examat_id DESC ";

		//use for MySQLi OOP
		$query = $conn->query($sql);
		
		while($row = $query->fetch(PDO::FETCH_ASSOC)){
			$contents .= "
			<tr>
				<td>".$row['exmne_fullname']."</td>
				<td>".$row['exmne_fullname']."</td>
				<td>".$row['exmne_majorship']."</td>
				<td>".$row['exmne_schlgrad']."</td>
				<td>".$row['exmne_email']."</td>
				<td>".$row['status']."</td>
			</tr>
			";
		}
		////////////////

		//use for MySQLi Procedural
		// $query = mysqli_query($conn, $sql);
		// while($row = mysqli_fetch_assoc($query)){
		// 	$contents .= "
		// 	<tr>
		// 		<td>".$row['id']."</td>
		// 		<td>".$row['firstname']."</td>
		// 		<td>".$row['lastname']."</td>
		// 		<td>".$row['address']."</td>
		// 	</tr>
		// 	";
		// }
		////////////////
		
		return $contents;
	 }

	require_once('tcpdf/tcpdf.php');  
    $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $pdf->SetCreator(PDF_CREATOR);  
    $pdf->SetTitle("CTE-Reviewer Center");  
    $pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $pdf->SetDefaultMonospacedFont('helvetica');  
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $pdf->SetMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);  
    $pdf->setPrintHeader(false);  
    $pdf->setPrintFooter(false);  
    $pdf->SetAutoPageBreak(TRUE, 10);  
    $pdf->SetFont('helvetica', '', 11);  
    $pdf->AddPage();  
    $content = '';  
    $content .= '
	<div align="center">        
	<img src="login-ui/images/pdf.jpg" alt="Logo" style="width:500px;  ">
	</div>
      	<h2 align="center">Examinee Result Reports</h2>
      	<h4>Examinee Result</h4>
      	<table border="1" cellspacing="0" cellpadding="3">  
           <tr>  
                <th width="18%">Fullname</th>
				<th width="7%">Gender</th>
				<th width="12%">Majorship</th>
				<th width="12%">Graduated</th> 
				<th width="18%">Email</th> 
				<th width="8%">Status</th> 
           </tr>  
      ';  
    $content .= generateRow();  
    $content .= '</table>';  
    $pdf->writeHTML($content);  
    $pdf->Output('examinee_tbl.pdf', 'I');
	

?>
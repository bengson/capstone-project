<!DOCTYPE html>
<html lang="en">
<head>
<title>CTE- Reviewer Center</title>
    
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="login-ui/image/png" href="	login-ui/images/logo1.png"/>
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/util.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/main.css">
	<script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100"  style="background-image: url(login-ui/images/back.jpg);  background-position: center; background-size: cover;  ">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(login-ui/images/index.png);">
					<span class="login100-form-title-1">
						Review Master
					</span>
				</div>
				<form method="post" id="adminLoginFrm" class="login100-form validate-form">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100"><i class="fas fa-envelope" style="font-size:30px"></i></span>
						<input class="input100" type="text" id="user" name="username" placeholder="Enter username">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
						<span class="label-input100"><i class="fas fa-lock" style="font-size:35px"></i></span>
						<input class="input100" type="password" id="id_password" name="pass" placeholder="Enter password">
						<i class="far fa-eye fa-lg" id="togglePassword" style="margin-left: 330px; position:absolute; top: 40%;  cursor: pointer;"></i>
						<span class="focus-input100"></span>
					</div>

					
					<br><br>

					<div class="container-login100-form-btn" align="right">
					
						<button type="submit" id="submit" class="login100-form-btn"><i class="fas fa-user-shield"></i>&nbsp&nbsp
							Login
						</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
						<a type=""style="color: gray; margin: top 20%;"  href="forgot.php"><b>
						Forgot password?</b></a>
								
					</div>
					<ul> 
						<br>
						Do you have an acount ? &nbsp	
						<a type=""  style="color: gray;"  href="enrollment.php" ><b>
							Sign Up!</b>	</a>
							</ul>	
						
				</form>
			</div>
		</div>
	</div>
	
	<script>
$("#submit") .click(function(){
      
        var user           = $("#user").val();
        var id_password              = $("#id_password").val();
        

        if(user == ''|| id_password == ''){

          Swal.fire({
           icon: 'warning',
           title: 'Ooops...',
           text: 'Please fill up Username/ Password. ',
           showConfirmButton: true,
           confirmButtonText:'Okay ',
           confirmButtonColor:'#1E90FF ',
           closeOnConfirm: false

           });

        }
});

</script>
	<script src="login-ui/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="login-ui/vendor/animsition/js/animsition.min.js"></script>
	<script src="login-ui/vendor/bootstrap/js/popper.js"></script>
	<script src="login-ui/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="login-ui/vendor/select2/select2.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/moment.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="login-ui/vendor/countdowntime/countdowntime.js"></script>
	<script src="login-ui/js/main.js"></script>
	<script>const togglePassword = document.querySelector('#togglePassword');
  const password = document.querySelector('#id_password');
 
  togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});</script>


</body>
</html>
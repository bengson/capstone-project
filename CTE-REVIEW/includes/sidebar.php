<?php 
  include("conn.php");
  include("query/selectData.php");
 ?>
<div class="app-sidebar sidebar-shadow fixed-sidebar fixed-header" style="background-color: #e1ad01;overflow-y: scroll;">
    <div class="app-header__logo">
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
<br>
<br>
            <div align="center">
                            <img src="assets/images/use.png" height="100" width="100" >
                           
                            </div>
                            <br>
                            <div align="center"style="color: black;"><b> <?php 
                                                echo strtoupper($selExmneeData['exmne_fullname']);
                                             ?></b></div>
         
         <br>

                <li class="app-sidebar__heading" ><a href="home.php"style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/speedometer.png"/>&nbsp&nbspDashboard</a></li>

                <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/test-passed.png"/>&nbsp AVAILABLE EXAMS</li>
                <li>
                <a href="#" style="color: black;">
                     
                     All Exams
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul >
                    <?php 
                        
                        if($selExam->rowCount() > 0)
                        {
                            while ($selExamRow = $selExam->fetch(PDO::FETCH_ASSOC)) { ?>
                                 <li>
                                 <a href="#" style="color: black;"id="startQuiz" data-id="<?php echo $selExamRow['ex_id']; ?>"  >
                                    <?php 
                                        $lenthOfTxt = strlen($selExamRow['ex_title']);
                                        if($lenthOfTxt >= 23)
                                        { ?>
                                            <?php echo substr($selExamRow['ex_title'], 0, 20);?>.....
                                        <?php }
                                        else
                                        {
                                            echo $selExamRow['ex_title'];
                                        }
                                     ?>
                                 </a>
                                 </li>
                            <?php }
                        }
                        else
                        { ?>
                            <a href="#" style="color: black;">
                                <i class="metismenu-icon"></i>No Exams @ the moment
                            </a>
                        <?php }
                     ?>


                </ul>
                </li>

                 <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/checked-user-male.png"/>&nbspTAKEN EXAMS</li>
                <li>
                  <?php 
                    $selTakenExam = $conn->query("SELECT * FROM exam_tbl et INNER JOIN exam_attempt ea ON et.ex_id = ea.exam_id WHERE exmne_id='$exmneId' ORDER BY ea.examat_id  ");

                    if($selTakenExam->rowCount() > 0)
                    {
                        while ($selTakenExamRow = $selTakenExam->fetch(PDO::FETCH_ASSOC)) { ?>
                            <a style="color: black;"href="home.php?page=result&id=<?php echo $selTakenExamRow['ex_id']; ?>" >
                               
                                <?php echo $selTakenExamRow['ex_title']; ?>
                            </a>
                        <?php }
                    }
                    else
                    { ?>
                        <a href="#" style="color: black;"class="pl-3">Have not taken exam </a>
                    <?php }
                    
                   ?>

                    
                </li>
                <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/guest-male.png"/>&nbsp MY ACCOUNT</li>
                <li>
                <a href="home.php?page=account" style="color: black;" #userModal" >
                                        
                   </i>User Information
                  </a>
              </li>

                <li class="app-sidebar__heading" style="color: black;"><img src="https://img.icons8.com/fluency-systems-regular/30/000000/comments--v1.png"/>&nbspFEEDBACK</li>
                <li>
                    <a href="#" style="color: black;" data-toggle="modal" data-target="#feedbacksModal" >
                        Add Feedback                       
                    </a>
                </li>
                
            </ul>
        </div>
    </div>
</div>  

<?php 
  include("../../../conn.php");
  $id = $_GET['id'];
 
  $selExmne = $conn->query("SELECT * FROM examinee_tbl WHERE exmne_id='$id' ")->fetch(PDO::FETCH_ASSOC);

 ?>

<fieldset style="width:543px;" >
	<legend><i class="facebox-header"><i class="edit large icon"></i>&nbsp;Update <b>( <?php echo strtoupper($selExmne['exmne_fullname']); ?> )</b></i></legend>
  <div class="col-md-12 mt-4">
<form method="post" id="updateExamineeFrm">
     <div class="form-group">
        <legend>Fullname</legend>
        <input type="hidden" name="exmne_id" value="<?php echo $id; ?>">
        <input type="" name="exFullname" class="form-control" required="" value="<?php echo $selExmne['exmne_fullname']; ?>" >
     </div>

     <div class="form-group">
        <legend>Gender</legend>
        <select class="form-control" name="exGender">
          <option value="<?php echo $selExmne['exmne_gender']; ?>"><?php echo $selExmne['exmne_gender']; ?></option>
          <option value="M">Male</option>
          <option value="F">Female</option>
        </select>
     </div>

     <div class="form-group">
        <legend>Majorship</legend>
        <select class="form-control" name="exmajorship">
          <option value="<?php echo $selExmne['exmne_majorship']; ?>"><?php echo $selExmne['exmne_majorship']; ?></option>
          <option value="English">English</option>
      <option value="Filipino">Filipino</option>
      <option value="Mathemathics">Mathemathics</option>
	  <option value="Social Science">Social Science</option>
	  <option value="Biological Science">Biological Science</option>
	  <option value="Physical Science">Physical Science</option>
	  <option value="MAPEH">MAPEH</option>
	  <option value="TLE">TLE</option>
	  <option value="Fishery Arts">Fishery Arts</option>
        </select>
     </div>


     <div class="form-group">
        <legend>Course</legend>
        <?php 
            $exmneCourse = $selExmne['exmne_course'];
            $selCourse = $conn->query("SELECT * FROM course_tbl WHERE cou_id='$exmneCourse' ")->fetch(PDO::FETCH_ASSOC);
         ?>
         <select class="form-control" name="exCourse">
           <option value="<?php echo $exmneCourse; ?>"><?php echo $selCourse['cou_name']; ?></option>
           <?php 
             $selCourse = $conn->query("SELECT * FROM course_tbl WHERE cou_id!='$exmneCourse' ");
             while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) { ?>
              <option value="<?php echo $selCourseRow['cou_id']; ?>"><?php echo $selCourseRow['cou_name']; ?></option>
            <?php  }
            ?>
         </select>
     </div>

     <div class="form-group">
        <legend>Graduate</legend>
        <input type="" name="exGraduate" class="form-control" required="" value="<?php echo $selExmne['exmne_schlgrad']; ?>" >
     </div>

     <div class="form-group">
        <legend>Email</legend>
        <input type="" name="exEmail" class="form-control" required="" value="<?php echo $selExmne['exmne_email']; ?>" >
     </div>

    
     
  <div class="form-group" align="right">
 
    <button type="submit" class="btn btn-sm btn-outline-primary">Update Now</button>
  </div>
</form>
  </div>
</fieldset>








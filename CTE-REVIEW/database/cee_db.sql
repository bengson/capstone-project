-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2022 at 02:10 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cee_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_acc`
--

CREATE TABLE `admin_acc` (
  `admin_id` int(11) NOT NULL,
  `admin_user` varchar(1000) NOT NULL,
  `admin_pass` varchar(1000) NOT NULL,
  `admin_fullname` varchar(255) NOT NULL,
  `admin_gender` varchar(255) NOT NULL,
  `admin_contact` varchar(255) NOT NULL,
  `admin_schlgrad` varchar(255) NOT NULL,
  `admin_yrgrad` varchar(255) NOT NULL,
  `admin_acad` varchar(255) NOT NULL,
  `admin_profession` varchar(255) NOT NULL,
  `admin_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_acc`
--

INSERT INTO `admin_acc` (`admin_id`, `admin_user`, `admin_pass`, `admin_fullname`, `admin_gender`, `admin_contact`, `admin_schlgrad`, `admin_yrgrad`, `admin_acad`, `admin_profession`, `admin_address`) VALUES
(1, 'admin', 'admin', '', '', '', '', '', '', '', ''),
(2, 'wawa', 'wawa', '', '', '', '', '', '', '', ''),
(3, 'barcelonjoshua6@gmail.com', '123', 'wawa', 'M', '13124', 'LNU', '234', 'Cumlaude', 'def', 'safasf'),
(4, 'abc@gmail.com', '12', 'joshua', 'M', '09658430979', 'Leyte Normal University', '2021-2022', 'Cumlaude', 'a', 'Sitio Canario Balocawehay');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `code` varchar(5) NOT NULL,
  `expire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `email`, `code`, `expire`) VALUES
(0, 'markjustinebengson.lnu@gmail.com', '86899', 1639191510);

-- --------------------------------------------------------

--
-- Table structure for table `course_tbl`
--

CREATE TABLE `course_tbl` (
  `cou_id` int(11) NOT NULL,
  `cou_name` varchar(1000) NOT NULL,
  `cou_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_tbl`
--

INSERT INTO `course_tbl` (`cou_id`, `cou_name`, `cou_created`) VALUES
(1, 'BSED', '2021-12-09 00:07:17'),
(2, 'BEED ', '2021-12-10 07:31:53');

-- --------------------------------------------------------

--
-- Table structure for table `examinee_tbl`
--

CREATE TABLE `examinee_tbl` (
  `exmne_id` int(11) NOT NULL,
  `exmne_fullname` varchar(1000) NOT NULL,
  `exmne_contact` varchar(50) NOT NULL,
  `exmne_course` varchar(1000) NOT NULL,
  `exmne_gender` varchar(1000) NOT NULL,
  `exmne_schlgrad` varchar(255) NOT NULL,
  `exmne_acad` varchar(11) NOT NULL,
  `exmne_take` varchar(255) NOT NULL,
  `exmne_majorship` varchar(255) NOT NULL,
  `exmne_payment` varchar(255) NOT NULL,
  `exmne_pic` varchar(50) NOT NULL,
  `exmne_fb` varchar(255) NOT NULL,
  `exmne_birthdate` varchar(1000) NOT NULL,
  `exmne_email` varchar(1000) NOT NULL,
  `exmne_password` varchar(1000) NOT NULL,
  `exmne_status` varchar(1000) NOT NULL DEFAULT 'active',
  `status` varchar(255) NOT NULL,
  `exmne_address` varchar(1000) NOT NULL,
  `exmne_yrgrad` varchar(1000) NOT NULL,
  `exmne_year_level` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examinee_tbl`
--

INSERT INTO `examinee_tbl` (`exmne_id`, `exmne_fullname`, `exmne_contact`, `exmne_course`, `exmne_gender`, `exmne_schlgrad`, `exmne_acad`, `exmne_take`, `exmne_majorship`, `exmne_payment`, `exmne_pic`, `exmne_fb`, `exmne_birthdate`, `exmne_email`, `exmne_password`, `exmne_status`, `status`, `exmne_address`, `exmne_yrgrad`, `exmne_year_level`) VALUES
(1, 'Mark Justine  Bengson', '09925647352', '1', 'M', 'LNU', 'Cumlaude', 'Yes', 'Mathemathics', 'Palawan', '611fa04687be2b9b20aa5663c1219e9e.jpg', 'Mark Justine', '1998-02-01', 'justineprensica@gmail.com ', 'password12', 'active', 'approved', '', '2020', ''),
(2, 'Luke zainex', '092474917377', '1', 'M', 'LNU', 'Cumlaude', 'Yes', 'English', 'Palawan', '20211210_203453.jpg', 'Luke', '2007-12-11', 'markjustinebengson.lnu@gmail.com ', '12345', 'active', 'approved', '', '2020', ''),
(3, 'Karl Joshua', '0967567563454', '2', 'M', 'LNU', 'Cumlaude', 'Yes', 'Filipino', 'Palawan', '2.jfif', 'karl', '1997-02-02', 'sagroup325@gmail.com ', '12345', 'active', 'approved', '', '2020', ''),
(4, 'Joshua Hernandez ', '0927382744729', '1', 'M', 'LNU', 'C', 'Yes', 'Mathemathics', '', '', '', '2000-12-12', 'joshuaher@gmail.com', '12345', 'active', 'approved', 'Naga-asan', '2020', ''),
(5, 'Winston', '0912313112', '1', 'M', 'EVSU', 'Magna Cumla', 'Yes', 'Physical Science', 'Gcash', '248323497_312027070407264_6092990333749871612_n.jp', 'asdsad', '2021-12-08', 'ton@gmail.com ', 'tonton', 'active', 'approved', '', '2018', ''),
(7, 'winston Lesigues', '09245354', '1', 'M', 'LNU', 'MC', 'Yes', 'Filipino', '', '', '', '2021-12-24', 'w@gmail.com', 'tonton', 'active', 'approved', 'Tac City', '2018', '');

-- --------------------------------------------------------

--
-- Table structure for table `exam_answers`
--

CREATE TABLE `exam_answers` (
  `exans_id` int(11) NOT NULL,
  `axmne_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `quest_id` int(11) NOT NULL,
  `exans_answer` varchar(1000) NOT NULL,
  `exans_status` varchar(1000) NOT NULL DEFAULT 'new',
  `exans_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_answers`
--

INSERT INTO `exam_answers` (`exans_id`, `axmne_id`, `exam_id`, `quest_id`, `exans_answer`, `exans_status`, `exans_created`) VALUES
(1, 1, 1, 16, 'Dynamic number access', 'new', '2021-12-11 06:24:55'),
(2, 1, 1, 20, '15', 'new', '2021-12-11 06:24:55'),
(3, 1, 1, 18, '32', 'new', '2021-12-11 06:24:55'),
(4, 1, 1, 17, '206', 'new', '2021-12-11 06:24:55'),
(5, 1, 1, 19, 'Mycology', 'new', '2021-12-11 06:24:55'),
(6, 1, 1, 7, 'of', 'new', '2021-12-11 06:24:55'),
(7, 1, 1, 9, 'What is your Surname', 'new', '2021-12-11 06:24:55'),
(8, 1, 1, 6, 'Versatile', 'new', '2021-12-11 06:24:55'),
(9, 1, 1, 8, 'moves', 'new', '2021-12-11 06:24:55'),
(10, 1, 1, 10, 'was', 'new', '2021-12-11 06:24:55'),
(11, 1, 1, 22, 'Strain', 'new', '2021-12-11 06:24:55'),
(12, 1, 1, 24, 'Two', 'new', '2021-12-11 06:24:55'),
(13, 1, 1, 21, 'Short distance run', 'new', '2021-12-11 06:24:55'),
(14, 1, 1, 23, 'Hamstring', 'new', '2021-12-11 06:24:55'),
(15, 1, 1, 25, 'Pulses', 'new', '2021-12-11 06:24:55'),
(16, 1, 1, 14, 'Gregorio Del Pilar', 'new', '2021-12-11 06:24:55'),
(17, 1, 1, 11, 'Intramuros', 'new', '2021-12-11 06:24:55'),
(18, 1, 1, 13, 'July 4', 'new', '2021-12-11 06:24:55'),
(19, 1, 1, 15, 'Albay', 'new', '2021-12-11 06:24:55'),
(20, 1, 1, 12, 'German', 'new', '2021-12-11 06:24:55'),
(21, 1, 1, 26, 'Norway', 'new', '2021-12-11 06:24:55'),
(22, 1, 1, 28, '15', 'new', '2021-12-11 06:24:55'),
(23, 1, 1, 30, 'New york city', 'new', '2021-12-11 06:24:55'),
(24, 1, 1, 27, '1925', 'new', '2021-12-11 06:24:55'),
(25, 1, 1, 29, 'Alamaba', 'new', '2021-12-11 06:24:55'),
(26, 1, 1, 1, '4', 'new', '2021-12-11 06:24:55'),
(27, 1, 1, 3, '1 846 000', 'new', '2021-12-11 06:24:55'),
(28, 1, 1, 5, '0.057', 'new', '2021-12-11 06:24:55'),
(29, 1, 1, 2, '0.4', 'new', '2021-12-11 06:24:55'),
(30, 1, 1, 4, '2100', 'new', '2021-12-11 06:24:55'),
(31, 2, 1, 16, 'Dynamic number access', 'new', '2021-12-11 06:41:25'),
(32, 2, 1, 20, '15', 'new', '2021-12-11 06:41:25'),
(33, 2, 1, 18, '32', 'new', '2021-12-11 06:41:25'),
(34, 2, 1, 17, '206', 'new', '2021-12-11 06:41:25'),
(35, 2, 1, 19, 'Mycology', 'new', '2021-12-11 06:41:25'),
(36, 2, 1, 7, 'of', 'new', '2021-12-11 06:41:25'),
(37, 2, 1, 9, 'What is your Surname', 'new', '2021-12-11 06:41:25'),
(38, 2, 1, 6, 'Versatile', 'new', '2021-12-11 06:41:25'),
(39, 2, 1, 8, 'move', 'new', '2021-12-11 06:41:25'),
(40, 2, 1, 10, 'was', 'new', '2021-12-11 06:41:25'),
(41, 2, 1, 22, 'Strain', 'new', '2021-12-11 06:41:25'),
(42, 2, 1, 24, 'Six', 'new', '2021-12-11 06:41:25'),
(43, 2, 1, 21, 'Rope climbing', 'new', '2021-12-11 06:41:25'),
(44, 2, 1, 23, 'Hamstring', 'new', '2021-12-11 06:41:25'),
(45, 2, 1, 25, 'Soya bean', 'new', '2021-12-11 06:41:25'),
(46, 2, 1, 14, 'Dr. Jose Rizal', 'new', '2021-12-11 06:41:25'),
(47, 2, 1, 11, 'Intramuros', 'new', '2021-12-11 06:41:25'),
(48, 2, 1, 13, 'July 4', 'new', '2021-12-11 06:41:25'),
(49, 2, 1, 15, 'Albay', 'new', '2021-12-11 06:41:25'),
(50, 2, 1, 12, 'German', 'new', '2021-12-11 06:41:25'),
(51, 2, 1, 26, 'Norway', 'new', '2021-12-11 06:41:25'),
(52, 2, 1, 28, '12', 'new', '2021-12-11 06:41:25'),
(53, 2, 1, 30, 'New york city', 'new', '2021-12-11 06:41:25'),
(54, 2, 1, 27, '1918', 'new', '2021-12-11 06:41:25'),
(55, 2, 1, 29, 'Arkansas', 'new', '2021-12-11 06:41:25'),
(56, 2, 1, 1, '4', 'new', '2021-12-11 06:41:25'),
(57, 2, 1, 3, '1 846 000', 'new', '2021-12-11 06:41:25'),
(58, 2, 1, 5, '0.057', 'new', '2021-12-11 06:41:25'),
(59, 2, 1, 2, '0.4', 'new', '2021-12-11 06:41:25'),
(60, 2, 1, 4, '2100', 'new', '2021-12-11 06:41:25'),
(61, 5, 1, 16, 'Dynamic number access', 'new', '2021-12-13 09:21:47'),
(62, 5, 1, 20, '15', 'new', '2021-12-13 09:21:47'),
(63, 5, 1, 18, '30', 'new', '2021-12-13 09:21:47'),
(64, 5, 1, 17, '201', 'new', '2021-12-13 09:21:47'),
(65, 5, 1, 19, 'Mycology', 'new', '2021-12-13 09:21:47'),
(66, 5, 1, 7, 'dont', 'new', '2021-12-13 09:21:47'),
(67, 5, 1, 9, 'What is your Surname', 'new', '2021-12-13 09:21:47'),
(68, 5, 1, 6, 'Exceptional', 'new', '2021-12-13 09:21:47'),
(69, 5, 1, 8, 'move', 'new', '2021-12-13 09:21:47'),
(70, 5, 1, 10, 'was', 'new', '2021-12-13 09:21:47'),
(71, 5, 1, 22, 'Sprain', 'new', '2021-12-13 09:21:47'),
(72, 5, 1, 24, 'Two', 'new', '2021-12-13 09:21:47'),
(73, 5, 1, 21, 'Vertical jump', 'new', '2021-12-13 09:21:47'),
(74, 5, 1, 23, 'Hamstring', 'new', '2021-12-13 09:21:47'),
(75, 5, 1, 25, 'Pulses', 'new', '2021-12-13 09:21:47'),
(76, 5, 1, 14, 'Dr. Jose Rizal', 'new', '2021-12-13 09:21:47'),
(77, 5, 1, 11, 'Manila', 'new', '2021-12-13 09:21:47'),
(78, 5, 1, 13, 'June 12', 'new', '2021-12-13 09:21:47'),
(79, 5, 1, 15, 'Cagayan', 'new', '2021-12-13 09:21:47'),
(80, 5, 1, 12, 'Japan', 'new', '2021-12-13 09:21:47'),
(81, 5, 1, 26, 'Germany', 'new', '2021-12-13 09:21:47'),
(82, 5, 1, 28, '15', 'new', '2021-12-13 09:21:47'),
(83, 5, 1, 30, 'San fransisco', 'new', '2021-12-13 09:21:47'),
(84, 5, 1, 27, '1925', 'new', '2021-12-13 09:21:47'),
(85, 5, 1, 29, 'Alamaba', 'new', '2021-12-13 09:21:47'),
(86, 5, 1, 1, '-4', 'new', '2021-12-13 09:21:47'),
(87, 5, 1, 3, '1 846 000', 'new', '2021-12-13 09:21:47'),
(88, 5, 1, 5, '0.057', 'new', '2021-12-13 09:21:47'),
(89, 5, 1, 2, '20', 'new', '2021-12-13 09:21:47'),
(90, 5, 1, 4, '10', 'new', '2021-12-13 09:21:47'),
(91, 3, 4, 47, '201', 'new', '2021-12-14 11:12:35'),
(92, 3, 4, 49, 'Mycology', 'new', '2021-12-14 11:12:35'),
(93, 3, 4, 46, 'Dynamic number access', 'new', '2021-12-14 11:12:35'),
(94, 3, 4, 48, '30', 'new', '2021-12-14 11:12:35'),
(95, 3, 4, 50, '15', 'new', '2021-12-14 11:12:35'),
(96, 3, 4, 39, 'What is your Surname', 'new', '2021-12-14 11:12:35'),
(97, 3, 4, 36, 'Exceptional', 'new', '2021-12-14 11:12:35'),
(98, 3, 4, 38, 'move', 'new', '2021-12-14 11:12:35'),
(99, 3, 4, 40, 'was', 'new', '2021-12-14 11:12:35'),
(100, 3, 4, 37, 'dont', 'new', '2021-12-14 11:12:35'),
(101, 3, 4, 54, 'Two', 'new', '2021-12-14 11:12:35'),
(102, 3, 4, 51, 'Vertical jump', 'new', '2021-12-14 11:12:35'),
(103, 3, 4, 53, 'Hamstring', 'new', '2021-12-14 11:12:35'),
(104, 3, 4, 55, 'Pulses', 'new', '2021-12-14 11:12:35'),
(105, 3, 4, 52, 'Strain', 'new', '2021-12-14 11:12:35'),
(106, 3, 4, 41, 'Manila', 'new', '2021-12-14 11:12:35'),
(107, 3, 4, 43, 'June 12', 'new', '2021-12-14 11:12:35'),
(108, 3, 4, 45, 'Cagayan', 'new', '2021-12-14 11:12:35'),
(109, 3, 4, 42, 'Japan', 'new', '2021-12-14 11:12:35'),
(110, 3, 4, 44, 'Dr. Jose Rizal', 'new', '2021-12-14 11:12:35'),
(111, 3, 4, 56, 'Norway', 'new', '2021-12-14 11:12:35'),
(112, 3, 4, 58, '12', 'new', '2021-12-14 11:12:35'),
(113, 3, 4, 60, 'New york city', 'new', '2021-12-14 11:12:35'),
(114, 3, 4, 57, '1918', 'new', '2021-12-14 11:12:35'),
(115, 3, 4, 59, 'Alamaba', 'new', '2021-12-14 11:12:35'),
(116, 3, 4, 32, '20', 'new', '2021-12-14 11:12:35'),
(117, 3, 4, 34, '15', 'new', '2021-12-14 11:12:35'),
(118, 3, 4, 31, '4', 'new', '2021-12-14 11:12:35'),
(119, 3, 4, 33, '1 847 001', 'new', '2021-12-14 11:12:35'),
(120, 3, 4, 35, '0.100', 'new', '2021-12-14 11:12:35'),
(121, 4, 1, 16, 'Dynamic number access', 'new', '2021-12-28 08:19:02'),
(122, 4, 1, 18, '30', 'new', '2021-12-28 08:19:02'),
(123, 4, 1, 20, '30', 'new', '2021-12-28 08:19:02'),
(124, 4, 1, 17, '300', 'new', '2021-12-28 08:19:02'),
(125, 4, 1, 19, 'energy', 'new', '2021-12-28 08:19:02'),
(126, 4, 1, 7, 'dont', 'new', '2021-12-28 08:19:02'),
(127, 4, 1, 9, 'What the your Surname', 'new', '2021-12-28 08:19:02'),
(128, 4, 1, 6, 'Wisdom', 'new', '2021-12-28 08:19:02'),
(129, 4, 1, 8, 'move', 'new', '2021-12-28 08:19:02'),
(130, 4, 1, 10, 'was', 'new', '2021-12-28 08:19:02'),
(131, 4, 1, 22, 'Strain', 'new', '2021-12-28 08:19:02'),
(132, 4, 1, 24, 'Six', 'new', '2021-12-28 08:19:02'),
(133, 4, 1, 21, 'Forward roll', 'new', '2021-12-28 08:19:02'),
(134, 4, 1, 23, 'enemius', 'new', '2021-12-28 08:19:02'),
(135, 4, 1, 25, 'Groundnut', 'new', '2021-12-28 08:19:02'),
(136, 4, 1, 14, 'Gregorio Del Pilar', 'new', '2021-12-28 08:19:02'),
(137, 4, 1, 11, 'Manila', 'new', '2021-12-28 08:19:02'),
(138, 4, 1, 13, 'June 12', 'new', '2021-12-28 08:19:02'),
(139, 4, 1, 15, 'Albay', 'new', '2021-12-28 08:19:02'),
(140, 4, 1, 12, 'German', 'new', '2021-12-28 08:19:02'),
(141, 4, 1, 26, 'Norway', 'new', '2021-12-28 08:19:02'),
(142, 4, 1, 28, '20', 'new', '2021-12-28 08:19:02'),
(143, 4, 1, 30, 'New york city', 'new', '2021-12-28 08:19:02'),
(144, 4, 1, 27, '1925', 'new', '2021-12-28 08:19:02'),
(145, 4, 1, 29, 'Arkansas', 'new', '2021-12-28 08:19:02'),
(146, 4, 1, 1, '1/4', 'new', '2021-12-28 08:19:02'),
(147, 4, 1, 3, '1 847 001', 'new', '2021-12-28 08:19:02'),
(148, 4, 1, 5, '0.100', 'new', '2021-12-28 08:19:02'),
(149, 4, 1, 2, '4', 'new', '2021-12-28 08:19:02'),
(150, 4, 1, 4, '15', 'new', '2021-12-28 08:19:02');

-- --------------------------------------------------------

--
-- Table structure for table `exam_attempt`
--

CREATE TABLE `exam_attempt` (
  `examat_id` int(11) NOT NULL,
  `exmne_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `examat_status` varchar(1000) NOT NULL DEFAULT 'used',
  `exam_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attempt`
--

INSERT INTO `exam_attempt` (`examat_id`, `exmne_id`, `exam_id`, `examat_status`, `exam_date`) VALUES
(1, 1, 1, 'used', ''),
(2, 2, 1, 'used', ''),
(3, 5, 1, 'used', ''),
(4, 3, 4, 'used', ''),
(5, 4, 1, 'used', '');

-- --------------------------------------------------------

--
-- Table structure for table `exam_question_tbl`
--

CREATE TABLE `exam_question_tbl` (
  `eqt_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_question` varchar(1000) NOT NULL,
  `exam_ch1` varchar(1000) NOT NULL,
  `exam_ch2` varchar(1000) NOT NULL,
  `exam_ch3` varchar(1000) NOT NULL,
  `exam_ch4` varchar(1000) NOT NULL,
  `exam_answer` varchar(1000) NOT NULL,
  `exam_status` varchar(1000) NOT NULL DEFAULT 'active',
  `exam_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ques_category` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_question_tbl`
--

INSERT INTO `exam_question_tbl` (`eqt_id`, `exam_id`, `exam_question`, `exam_ch1`, `exam_ch2`, `exam_ch3`, `exam_ch4`, `exam_answer`, `exam_status`, `exam_date`, `ques_category`, `category_id`) VALUES
(1, 1, 'If Logx (1 / 8) = - 3 / 2, then x is equal to', '-4', '4', '1/4', '5', '4', 'active', '2021-12-11 05:04:46', 3, NULL),
(2, 1, '20 % of 2 is equal to', '20', '4', '0.4', '0.5', '0.4', 'active', '2021-12-11 05:05:28', 3, NULL),
(3, 1, 'The population of a country increased by an average of 2% per year from 2000 to 2003. If the population of this country was 2 000 000 on December 31, 2003, then the population of this country on January 1, 2000, to the nearest thousand would have been', '1 846 000', '1 847 001', '1 888 000', '1 000 23', '1 846 000', 'active', '2021-12-11 05:06:59', 3, NULL),
(4, 1, 'A school committee consists of 2 teachers and 4 students. The number of different committees that can be formed from 5 teachers and 10 students is', '10', '15', '2100', '8', '2100', 'active', '2021-12-11 05:08:49', 3, NULL),
(5, 1, 'The probability that an electronic device produced by a company does not function properly is equal to 0.1. If 10 devices are bought, then the probability, to the nearest thousandth, that 7 devices function properly is', '0.057', '0.100', '0.120', '0.055', '0.057', 'active', '2021-12-11 05:10:25', 3, NULL),
(6, 1, 'One who possesses many talents\r\n\r\nOne word substitute for the given word is?', 'Exceptional', 'Versatile', 'Wisdom', 'Nubile', 'Versatile', 'active', '2021-12-11 05:24:43', 1, NULL),
(7, 1, 'which part of the given sentence has an error?  She dont speak of either German or French', 'dont', 'of', 'or', 'speak', 'of', 'active', '2021-12-11 05:26:56', 1, NULL),
(8, 1, 'Choose the correct verb form from those in brackets.  The earth round the sun.', 'move', 'moved', 'moves', 'None of the above', 'moves', 'active', '2021-12-11 05:29:18', 1, NULL),
(9, 1, 'What your Surname is?', 'What is your Surname', 'What the your Surname', 'Your Surname is what?', 'None of the above', 'What is your Surname', 'active', '2021-12-11 05:31:50', 1, NULL),
(10, 1, 'I ......... born in 1992.', 'was', 'were', 'is', 'am', 'was', 'active', '2021-12-11 05:33:50', 1, NULL),
(11, 1, 'Which city is known as the â€œWalled City?â€', 'Manila', 'Intramuros', 'Makita', 'Davao', 'Intramuros', 'active', '2021-12-11 05:45:02', 2, NULL),
(12, 1, 'Which country occupied the Philippines during World War II?', 'Japan', 'America', 'German', 'Philippines', 'America', 'active', '2021-12-11 05:46:22', 2, NULL),
(13, 1, 'Philippines Independence Day is celebrated on what date?', 'June 12', 'July 4', 'December 25', 'July 11', 'July 4', 'active', '2021-12-11 05:47:24', 2, NULL),
(14, 1, 'Who was known as the \"Hero of Tirad Pass?\"', 'Dr. Jose Rizal', 'Andress Bonifacio', 'Gregorio Del Pilar', 'Lapu-Lapu', 'Gregorio Del Pilar', 'active', '2021-12-11 05:51:48', 2, NULL),
(15, 1, 'Mayon Volcano is located in which province?', 'Cagayan', 'Leyte', 'Albay', 'Zambales', 'Albay', 'active', '2021-12-11 05:52:37', 2, NULL),
(16, 1, 'What does DNA stand for?', 'Dynamic number access', 'Deoxyribonucleic acid', 'Deoxyribonucleic ', 'acid', 'Deoxyribonucleic acid', 'active', '2021-12-11 05:55:24', 4, NULL),
(17, 1, 'How many bones are in the human body?', '201', '100', '206', '300', '206', 'active', '2021-12-11 05:56:21', 4, NULL),
(18, 1, 'How many teeth does an adult human have?', '30', '20', '32', '35', '32', 'active', '2021-12-11 05:57:27', 4, NULL),
(19, 1, 'What is the study of mushrooms called?', 'Mycology', 'Biology', 'Astronomy', 'energy', 'Mycology', 'active', '2021-12-11 05:58:36', 4, NULL),
(20, 1, 'How many vertebrae does the average human possess?', '15', '30', '33', '14', '33', 'active', '2021-12-11 05:59:20', 4, NULL),
(21, 1, 'Which of the following activities measures dynamic strength ?', 'Vertical jump', 'Short distance run', 'Forward roll', 'Rope climbing', 'Rope climbing', 'active', '2021-12-11 06:10:11', 5, NULL),
(22, 1, 'In the technical terms, muscle pull is known asâ€“', 'Sprain', 'Strain', 'Abrasion', 'Contusion', 'Strain', 'active', '2021-12-11 06:11:37', 5, NULL),
(23, 1, 'The back thigh muscles are also known asâ€“', 'Hamstring', 'Gluteal', 'Gastro', 'enemius', 'Hamstring', 'active', '2021-12-11 06:13:50', 5, NULL),
(24, 1, 'How many major Salivary glands are there in the human body?', 'Two', 'Four', 'Six', 'Five', 'Six', 'active', '2021-12-11 06:14:49', 5, NULL),
(25, 1, 'The richest source of protein isâ€“', 'Pulses', 'Soya bean', 'Egg', 'Groundnut', 'Soya bean', 'active', '2021-12-11 06:16:03', 5, NULL),
(26, 1, 'Which of these nations was neutral in World War I?', 'Germany', 'Norway', 'America', 'Japan', 'Norway', 'active', '2021-12-11 06:17:37', 6, NULL),
(27, 1, 'World War I ended in?', '1925', '1918', '1920', '1970', '1918', 'active', '2021-12-11 06:18:45', 6, NULL),
(28, 1, 'How many republics made up the former Soviet Union?', '15', '12', '17', '20', '15', 'active', '2021-12-11 06:19:34', 6, NULL),
(29, 1, 'In what American state would you find Denali?', 'Alamaba', 'Alaska', 'Arkansas', 'Arizona', 'Alaska', 'active', '2021-12-11 06:20:58', 6, NULL),
(30, 1, 'In which city would you find the Statue of Liberty?', 'San fransisco', 'New york city', 'Washington DC', 'Philadelphia', 'New york city', 'active', '2021-12-11 06:22:15', 6, NULL),
(31, 4, 'If Logx (1 / 8) = - 3 / 2, then x is equal to', '-4', '4', '1/4', '5', '4', 'active', '2021-12-11 12:04:46', 3, NULL),
(32, 4, '20 % of 2 is equal to', '20', '4', '0.4', '0.5', '0.4', 'active', '2021-12-11 12:05:28', 3, NULL),
(33, 4, 'The population of a country increased by an average of 2% per year from 2000 to 2003. If the population of this country was 2 000 000 on December 31, 2003, then the population of this country on January 1, 2000, to the nearest thousand would have been', '1 846 000', '1 847 001', '1 888 000', '1 000 23', '1 846 000', 'active', '2021-12-11 12:06:59', 3, NULL),
(34, 4, 'A school committee consists of 2 teachers and 4 students. The number of different committees that can be formed from 5 teachers and 10 students is', '10', '15', '2100', '8', '2100', 'active', '2021-12-11 12:08:49', 3, NULL),
(35, 4, 'The probability that an electronic device produced by a company does not function properly is equal to 0.1. If 10 devices are bought, then the probability, to the nearest thousandth, that 7 devices function properly is', '0.057', '0.100', '0.120', '0.055', '0.057', 'active', '2021-12-11 12:10:25', 3, NULL),
(36, 4, 'One who possesses many talents\r\n\r\nOne word substitute for the given word is?', 'Exceptional', 'Versatile', 'Wisdom', 'Nubile', 'Versatile', 'active', '2021-12-11 12:24:43', 1, NULL),
(37, 4, 'which part of the given sentence has an error?  She dont speak of either German or French', 'dont', 'of', 'or', 'speak', 'of', 'active', '2021-12-11 12:26:56', 1, NULL),
(38, 4, 'Choose the correct verb form from those in brackets.  The earth round the sun.', 'move', 'moved', 'moves', 'None of the above', 'moves', 'active', '2021-12-11 12:29:18', 1, NULL),
(39, 4, 'What your Surname is?', 'What is your Surname', 'What the your Surname', 'Your Surname is what?', 'None of the above', 'What is your Surname', 'active', '2021-12-11 12:31:50', 1, NULL),
(40, 4, 'I ......... born in 1992.', 'was', 'were', 'is', 'am', 'was', 'active', '2021-12-11 12:33:50', 1, NULL),
(41, 4, 'Which city is known as the â€œWalled City?â€', 'Manila', 'Intramuros', 'Makita', 'Davao', 'Intramuros', 'active', '2021-12-11 12:45:02', 2, NULL),
(42, 4, 'Which country occupied the Philippines during World War II?', 'Japan', 'America', 'German', 'Philippines', 'America', 'active', '2021-12-11 12:46:22', 2, NULL),
(43, 4, 'Philippines Independence Day is celebrated on what date?', 'June 12', 'July 4', 'December 25', 'July 11', 'July 4', 'active', '2021-12-11 12:47:24', 2, NULL),
(44, 4, 'Who was known as the \"Hero of Tirad Pass?\"', 'Dr. Jose Rizal', 'Andress Bonifacio', 'Gregorio Del Pilar', 'Lapu-Lapu', 'Gregorio Del Pilar', 'active', '2021-12-11 12:51:48', 2, NULL),
(45, 4, 'Mayon Volcano is located in which province?', 'Cagayan', 'Leyte', 'Albay', 'Zambales', 'Albay', 'active', '2021-12-11 12:52:37', 2, NULL),
(46, 4, 'What does DNA stand for?', 'Dynamic number access', 'Deoxyribonucleic acid', 'Deoxyribonucleic ', 'acid', 'Deoxyribonucleic acid', 'active', '2021-12-11 12:55:24', 4, NULL),
(47, 4, 'How many bones are in the human body?', '201', '100', '206', '300', '206', 'active', '2021-12-11 12:56:21', 4, NULL),
(48, 4, 'How many teeth does an adult human have?', '30', '20', '32', '35', '32', 'active', '2021-12-11 12:57:27', 4, NULL),
(49, 4, 'What is the study of mushrooms called?', 'Mycology', 'Biology', 'Astronomy', 'energy', 'Mycology', 'active', '2021-12-11 12:58:36', 4, NULL),
(50, 4, 'How many vertebrae does the average human possess?', '15', '30', '33', '14', '33', 'active', '2021-12-11 12:59:20', 4, NULL),
(51, 4, 'Which of the following activities measures dynamic strength ?', 'Vertical jump', 'Short distance run', 'Forward roll', 'Rope climbing', 'Rope climbing', 'active', '2021-12-11 13:10:11', 5, NULL),
(52, 4, 'In the technical terms, muscle pull is known asâ€“', 'Sprain', 'Strain', 'Abrasion', 'Contusion', 'Strain', 'active', '2021-12-11 13:11:37', 5, NULL),
(53, 4, 'The back thigh muscles are also known asâ€“', 'Hamstring', 'Gluteal', 'Gastro', 'enemius', 'Hamstring', 'active', '2021-12-11 13:13:50', 5, NULL),
(54, 4, 'How many major Salivary glands are there in the human body?', 'Two', 'Four', 'Six', 'Five', 'Six', 'active', '2021-12-11 13:14:49', 5, NULL),
(55, 4, 'The richest source of protein isâ€“', 'Pulses', 'Soya bean', 'Egg', 'Groundnut', 'Soya bean', 'active', '2021-12-11 13:16:03', 5, NULL),
(56, 4, 'Which of these nations was neutral in World War I?', 'Germany', 'Norway', 'America', 'Japan', 'Norway', 'active', '2021-12-11 13:17:37', 6, NULL),
(57, 4, 'World War I ended in?', '1925', '1918', '1920', '1970', '1918', 'active', '2021-12-11 13:18:45', 6, NULL),
(58, 4, 'How many republics made up the former Soviet Union?', '15', '12', '17', '20', '15', 'active', '2021-12-11 13:19:34', 6, NULL),
(59, 4, 'In what American state would you find Denali?', 'Alamaba', 'Alaska', 'Arkansas', 'Arizona', 'Alaska', 'active', '2021-12-11 13:20:58', 6, NULL),
(60, 4, 'In which city would you find the Statue of Liberty?', 'San fransisco', 'New york city', 'Washington DC', 'Philadelphia', 'New york city', 'active', '2021-12-11 13:22:15', 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exam_tbl`
--

CREATE TABLE `exam_tbl` (
  `ex_id` int(11) NOT NULL,
  `cou_id` int(11) NOT NULL,
  `ex_title` varchar(1000) NOT NULL,
  `ex_time_limit` varchar(1000) NOT NULL,
  `ex_questlimit_display` int(11) NOT NULL,
  `ex_description` varchar(1000) NOT NULL,
  `ex_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_tbl`
--

INSERT INTO `exam_tbl` (`ex_id`, `cou_id`, `ex_title`, `ex_time_limit`, `ex_questlimit_display`, `ex_description`, `ex_created`) VALUES
(1, 1, 'Pre-Test for BSED (General Education)', '20', 30, 'Good luck!', '2021-12-11 06:20:07'),
(4, 2, 'Pre-Test for BEED (Professional Education)', '20', 30, 'Good luck!', '2021-12-11 06:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks_tbl`
--

CREATE TABLE `feedbacks_tbl` (
  `fb_id` int(11) NOT NULL,
  `exmne_id` int(11) NOT NULL,
  `fb_exmne_as` varchar(1000) NOT NULL,
  `fb_feedbacks` varchar(1000) NOT NULL,
  `fb_date` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_category`
--

CREATE TABLE `question_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `category_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `question_category`
--

INSERT INTO `question_category` (`category_id`, `category_name`, `category_created`) VALUES
(1, 'English', '2021-10-06 07:50:20'),
(2, 'Filipino', '2021-10-06 07:50:20'),
(3, 'Math', '2021-10-06 07:22:22'),
(4, 'Science', '2021-11-22 04:44:29'),
(5, 'PE', '2021-12-02 08:12:51'),
(6, 'Values Education', '2021-12-11 07:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `review_master_acc`
--

CREATE TABLE `review_master_acc` (
  `review_master_id` int(11) NOT NULL,
  `review_master_user` varchar(255) NOT NULL,
  `review_master_pass` varchar(255) NOT NULL,
  `review_master_fullname` varchar(255) NOT NULL,
  `review_master_mname` varchar(255) NOT NULL,
  `review_master_gender` varchar(255) NOT NULL,
  `review_master_contact` varchar(255) NOT NULL,
  `review_master_schlgrad` varchar(255) NOT NULL,
  `review_master_yrgrad` varchar(255) NOT NULL,
  `review_master_acad` varchar(255) NOT NULL,
  `review_master_profession` varchar(255) NOT NULL,
  `review_master_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `review_master_acc`
--

INSERT INTO `review_master_acc` (`review_master_id`, `review_master_user`, `review_master_pass`, `review_master_fullname`, `review_master_mname`, `review_master_gender`, `review_master_contact`, `review_master_schlgrad`, `review_master_yrgrad`, `review_master_acad`, `review_master_profession`, `review_master_address`) VALUES
(1, 'barcelonjoshua6@gmail.com', '1', 'review', 'master', 'M', '1231241', 'Leyte Normal University', '2021-2022', 'Cumlaude', 'a', 'Sitio Canario Balocawehay'),
(2, 'Sirabac@gmail.com', '12345', 'Sir Abac', 'D', 'M', '6382923', 'LNU', '2020', 'Cumlaude', 'Teacher', 'Brgy Victory');

-- --------------------------------------------------------

--
-- Stand-in structure for view `score_per_category_with_answer_checking`
-- (See below for the actual view)
--
CREATE TABLE `score_per_category_with_answer_checking` (
`category_score` bigint(21)
,`exmne_fullname` varchar(1000)
,`exmne_id` int(11)
,`ex_title` varchar(1000)
,`ex_id` int(11)
,`ques_category` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `score_per_category_with_answer_checking`
--
DROP TABLE IF EXISTS `score_per_category_with_answer_checking`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `score_per_category_with_answer_checking`  AS SELECT count(`s`.`exmne_fullname`) AS `category_score`, `s`.`exmne_fullname` AS `exmne_fullname`, `s`.`exmne_id` AS `exmne_id`, `e`.`ex_title` AS `ex_title`, `e`.`ex_id` AS `ex_id`, `q`.`ques_category` AS `ques_category` FROM (((`exam_question_tbl` `q` join `exam_tbl` `e` on(`e`.`ex_id` = `q`.`exam_id`)) join `exam_answers` `a` on(`a`.`quest_id` = `q`.`eqt_id`)) join `examinee_tbl` `s` on(`s`.`exmne_id` = `a`.`axmne_id`)) WHERE `q`.`exam_answer` = `a`.`exans_answer` GROUP BY `s`.`exmne_id`, `q`.`exam_id`, `q`.`ques_category` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_acc`
--
ALTER TABLE `admin_acc`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `course_tbl`
--
ALTER TABLE `course_tbl`
  ADD PRIMARY KEY (`cou_id`);

--
-- Indexes for table `examinee_tbl`
--
ALTER TABLE `examinee_tbl`
  ADD PRIMARY KEY (`exmne_id`);

--
-- Indexes for table `exam_answers`
--
ALTER TABLE `exam_answers`
  ADD PRIMARY KEY (`exans_id`);

--
-- Indexes for table `exam_attempt`
--
ALTER TABLE `exam_attempt`
  ADD PRIMARY KEY (`examat_id`);

--
-- Indexes for table `exam_question_tbl`
--
ALTER TABLE `exam_question_tbl`
  ADD PRIMARY KEY (`eqt_id`);

--
-- Indexes for table `exam_tbl`
--
ALTER TABLE `exam_tbl`
  ADD PRIMARY KEY (`ex_id`);

--
-- Indexes for table `feedbacks_tbl`
--
ALTER TABLE `feedbacks_tbl`
  ADD PRIMARY KEY (`fb_id`);

--
-- Indexes for table `review_master_acc`
--
ALTER TABLE `review_master_acc`
  ADD PRIMARY KEY (`review_master_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_acc`
--
ALTER TABLE `admin_acc`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course_tbl`
--
ALTER TABLE `course_tbl`
  MODIFY `cou_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `examinee_tbl`
--
ALTER TABLE `examinee_tbl`
  MODIFY `exmne_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `exam_answers`
--
ALTER TABLE `exam_answers`
  MODIFY `exans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `exam_attempt`
--
ALTER TABLE `exam_attempt`
  MODIFY `examat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `exam_question_tbl`
--
ALTER TABLE `exam_question_tbl`
  MODIFY `eqt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `exam_tbl`
--
ALTER TABLE `exam_tbl`
  MODIFY `ex_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `feedbacks_tbl`
--
ALTER TABLE `feedbacks_tbl`
  MODIFY `fb_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `review_master_acc`
--
ALTER TABLE `review_master_acc`
  MODIFY `review_master_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

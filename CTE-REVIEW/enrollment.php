<?php include 'conn.php';?>
<!DOCTYPE html>
<html lang="en">

<style>
    html {
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }
   </style>
<head>
  <title>CTE- Reviewer Center</title>
  <meta charset="UTF-8">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="login-ui/image/png" href="login-ui/images/logo.png"/>
  
  <link rel="stylesheet" type="text/css" href="login-ui/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="login-ui/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
  <link rel="stylesheet" type="text/css" href="login-ui/vendor/animate/animate.css">
  <link rel="stylesheet" type="text/css" href="login-ui/vendor/css-hamburgers/hamburgers.min.css">
  <link rel="stylesheet" type="text/css" href="login-ui/vendor/select2/select2.min.css">
  <link rel="stylesheet" type="text/css" href="login-ui/vendor/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="login-ui/css/util.css">
  <link rel="stylesheet" type="text/css" href="login-ui/css/main.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<script src="js/script.js" defer></script>
<body>
  
  <div class="limiter">
    <div class="container-login100" style="background-image: url(login-ui/images/back.jpg);  background-position: center; background-size: cover; display: flex; ">
      <div class="wrap-login100">
        <div class="login100-form-title" style="background-image: url(login-ui/images/index.png);">
          <span class="login100-form-title-1">
            Enrollment 
          </span>
        </div>


   <!--enrollment starts-->
   <form class="form needs-validation"  method="POST"  enctype="multipart/form-data" novalidate>

      
      <!-- Progress bar -->
      <div class="progressbar">
        <div class="progress" id="progress"></div>
        
        <div
          class="progress-step progress-step-active"
          data-title="Personal "
        ></div>
        <div class="progress-step" data-title="Contact"></div>
        <div class="progress-step" data-title="School"></div>
        <div class="progress-step" data-title="Payment"></div>
        <div class="progress-step" data-title="Password"></div>
      </div>

      <!-- Steps -->
      <div class="form-step form-step-active">
      <center><label for=""><p><FONT size="5">Personal Information</p></FONT></label></center><br>
      
        <div class="form-group ">
         <label for="validationCustom01"><p><font size="3">Full Name</font></p> </label> 
          <input type="text" name="username" id="validationCustom01" class="form-control" required/>
          <div class="invalid-feedback">
          Please input fullname required
          </div>
        </div>

        <div class="form-group ">
       <label for="validationCustom03"> <p><font size="3">Birthdate</font></p></label>
            <input type="date" name="bdate" id="validationCustom03" class="form-control" required />
            <div class="invalid-feedback">
            Please input birthday required
          </div>
          </div>
        <div class="form-group ">
       <label for="validationCustom04"><p><font size="3">Gender</font></p></label> 
    <select class="form-control" name="gender" id="validationCustom04" required> 
   
    <option  value="" selected>Choose...</option>
      <option value="M">Male</option>
      <option value="F">Female</option>  
            </select>
            <div class="invalid-feedback">
            Please input gender required
          </div>
  </div>
  <br>
        <div class="btns-group">
          <a href="index.php" class="btn btn-prev ">Back</a>
          <a href="#" class="btn btn-next ">Next</a>
        </div>
      </div>
      
      <div class="form-step">
      <center><p><FONT size="5">Contact Information</p></FONT></center><br>
        <div class="form-group">
        <label for="validationCustom05"><p><font size="3">Contact Number (Globe or TM Only)</font></p></label>
          <input type="text" class="form-control" name="phno" id="validationCustom05" required/>
          <div class="invalid-feedback">
          Please input phone number required
          </div>
        </div>
        <div class="form-group">
        <label for="validationCustom05"><p><font size="3">Home Address</font></p></label>
          <input type="text" class="form-control" name="address" id="validationCustom05" required/>
          <div class="invalid-feedback">
          Please input address required
          </div>
        </div>
        <br>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      
      
      <div class="form-step">
      <center><p><FONT size="5">School Information</p></FONT></center><br>
      
    <div class="form-group ">
    <label for="validationCustom08"><p><font size="3">Degree</font></p></label>
    <select class="form-control" name="degree" id="validationCustom08" required>
    
    <option  value="" selected>Choose...</option>
    <?php 
                $selCourse = $conn->query("SELECT * FROM course_tbl ORDER BY cou_id asc");
                while ($selCourseRow = $selCourse->fetch(PDO::FETCH_ASSOC)) { ?>
                  <option value="<?php echo $selCourseRow['cou_id']; ?>"><?php echo $selCourseRow['cou_name']; ?></option>
                <?php }
               ?>
    </select>
    <div class="invalid-feedback">
    Please input degree required
          </div>
  </div>
  <div class="form-group ">
  <label for="validationCustom09"><p><font size="3">School Graduate</font></p></label>
    <input type="text" class="form-control" id="validationCustom09" name="school_grad" required>
    <div class="invalid-feedback">
    Please input school graduate required
          </div>
  
   </div>
   
  <div class="form-group ">
  <label for=""><p><font size="3">Year Graduate</font></p></label>
    <input type="text" class="form-control" id="year_grad" name="year_grad" required >
    <div class="invalid-feedback">
    Please input year graduate required
          </div>
  </div>
  <div class="form-group ">
 <label for="validationCustom10"> <p><font size="3">Academic Award</font></p></label>
    <select class="form-control" name="award" id="validationCustom10" required>
    
    <option  value="" selected>Choose...</option>
      <option value="Cumlaude">Cumlaude</option>
      <option value="Magna Cumlaude">Magna Cumlaude</option>
      <option value="Summa Cumlaude">Summa Cumlaude</option>
      <option value="None">None</option>
    </select>
    <div class="invalid-feedback">
    Please input Academic award required
          </div>
  </div>
  
  
  <div class="form-group ">
 <label for="validationCustom11"><p><font size="3">First Taker</font></p></label> 
    <select class="form-control" name="take" id="validationCustom11" required>
   
    <option   value="" selected>Choose...</option>
      <option  value="Yes">Yes</option>
      <option  value="No">No</option>
    
    </select>
    <div class="invalid-feedback">
    Please input Take required
          </div>
  </div>
  <div class="form-group ">
  <label for="validationCustom12"><p><font size="3">Majorship</font></p></label>
    <select class="form-control" name="majorship" id="validationCustom12" required>
    
    <option   value="" selected>Choose...</option>
      <option value="English">English</option>
      <option value="Filipino">Filipino</option>
      <option value="Mathemathics">Mathemathics</option>
    <option value="Social Science">Social Science</option>
    <option value="Biological Science">Biological Science</option>
    <option value="Physical Science">Physical Science</option>
    <option value="MAPEH">MAPEH</option>
    <option value="TLE">TLE</option>
    <option value="Fishery Arts">Fishery Arts</option>
    <option value="TLE">Content</option>
    <option value="Fishery Arts">SPED</option>
    <option value="Fishery Arts">Pre-school</option>
    </select>
    <div class="invalid-feedback">
    Please input majorship required
          </div>
  </div>
  <br>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      <div class="form-step">
      <center><p><FONT size="5">Payment Information</p></FONT></center><br>
      <label><b> Mode of Payment:</b><p> 1. Personal Transaction at LNU Cashier's Office - Present QR Code at LNU Gate
    (Note: Go first to LNU CTE Office for the Registration Form to be presented at LNU Cashier)</p>

<p>2. Palawan Express Padala with details below:
          Name: Ruby Mae H. Pe���aranda
          Mobile Num: 0967-253-0502
          Purpose of Transaction: LET Review Registration Fee</p>

<p>3. Gcash 
     (Note: Add 2% of the Total Amount of Payment for the Cash Out Service Fee)
     Send To: 09672530502
     Amount: Payment  + 2% Service Fee = Total Amount
     Message: 
        Name of Reviewee: _________________</p>

<p>Send picture of payment receipt to ctereview2021@gmail.com for confirmation.</p></label>

<div class="col-md-4 mb-3">
<label for="validationCustom13"><p><font size="3">Payment Method</font></p></label>
    <select class="form-control" name="pay" id="validationCustom13" required>
  <option  value="" selected>Choose...</option>
      <option value="Personal Transaction at LNU Cashier's Office"> Personal Transaction at LNU Cashier's Office</option>
      <option value="Palawan">Palawan</option>
    <option value="Gcash">Gcash</option>     
    </select>
    <div class="invalid-feedback">
    Please input payment required
          </div>
  </div>
  <div class="form-group">
 <label for="validationCustom14"><p><font size="3">Picture of proof of payment</font></p></label> 
    <input type="file" class="form-control" name="image" id="validationCustom14" required>
    <div class="invalid-feedback">
    Please input proof of payment required
          </div>
  </div>
  <div class=" form-group ">
  <label for="validationCustom15"><p><font size="3">Facebook Profile Name: (for Groupchat)</font></p></label>
      
      <input type="text" class="form-control" name="fb_name" id="validationCustom15" placeholder="Facebook Profile Name" required>
      <div class="invalid-feedback">
      Please input Facebook name required
          </div>
     
    </div>
   
  

      </label>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      
      <div class="form-step">
      <center><p><FONT size="5">Account Information</p></FONT></center><br>
      <div class="form-group">
      <label for="validationCustom06"> <p><font size="3">Email</font></p></label> 
          <input type="text"  class="form-control" name="email" id="validationCustom06"  required/>
          <div class="invalid-feedback">
          Please input email required
          </div>
        </div>
        <div class="form-group">
        <label for="validationCustom16"><p><font size="3">Password</font></p></label>
          <input class="form-control" type="password" name="password" id="validationCustom16" required />
          <div class="invalid-feedback">
          Please input password required
          </div>
</div>
<div class="form-group">
<label for="validationCustom17"><p><font size="3">Confirm Password</font></p></label>
          <input class="form-control" type="password" name="Cpassword" id="validationCustom17" required />
          <div class="invalid-feedback">
            Please input confirm password required
          </div>
</div>
<div>
<p><font size="3"> <i class="fas fa-exclamation-circle"></i>&nbsp<b> Disclaimer</b></font></p>

<p><font size="3">All information entered in this site (including names ,email and address) will be used
  exclusively for the stated purposes of this site and will not be made available for any other  purposes
  on to any other party. By checking the box below, you agree to be bound with the provisions of the 
  <b>Data Privacy Act 2012</b>
</font></p>
</div>

<div class="form-check form-switch">
<input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
<label class="form-check-label" for="invalidCheck"><p>Agree to terms and conditons</p></label>
<div class="invalid-feedback">
            Please check the box
          </div>
</div>

<br>

        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <input type="submit" value="Submit"  name ="submit" id="submit" class="btn" />
        </div>
      </div>
      
    </form>
<!--enrollment end-->

<script>
(function(){
  'use strict'
  var forms = document.querySelectorAll('.needs-validation')

  Array.prototype.slice.call(forms)
  .forEach(function(form){

    form.addEventListener('submit', function (event){
      if (!form.checkValidity()){
        event.preventDefault()
        event.stopPropagation()
      }
form.classList.add('was-validated')
    },false)
  })

})()

</script>

<?php
include 'connection.php';

if(isset($_POST['submit'])){


  $username      = $_POST['username'];
  $bdate         = $_POST['bdate'];
  $gender        = $_POST['gender'];
  $phno          = $_POST['phno'];
  $address       = $_POST['address'];
  $email         = $_POST['email'];
  $degree        = $_POST['degree'];
  $school_grad   = $_POST['school_grad'];
  $year_grad     = $_POST['year_grad'];
  $award         = $_POST['award'];
  $take          = $_POST['take'];
  $majorship     = $_POST['majorship'];
  $pay           = $_POST['pay'];
  $fb_name       = $_POST['fb_name'];
  $password      = $_POST['password'];
  $image         = $_FILES['image']['name'];

  $select = "SELECT * FROM examinee_tbl WHERE exmne_email = '$email'";
  $result = mysqli_query($conn, $select);


  if(mysqli_num_rows($result) > 0){
    echo "<script>
         
    Swal.fire({
         icon: 'error',
         title: 'Ooops...',
         text: 'Email is already exist. Try again!',
         showConfirmButton: true,
         confirmButtonText:'Okay ',
         confirmButtonColor:'#1E90FF ',
         closeOnConfirm: false

         })

     </script>";
  }else{
      $register = "INSERT INTO examinee_tbl (exmne_fullname, exmne_birthdate, exmne_gender, exmne_contact, exmne_email, exmne_course, exmne_schlgrad,  exmne_yrgrad, exmne_acad, exmne_take, exmne_majorship, exmne_payment, exmne_pic, exmne_fb, exmne_password, status) VALUES ('$username', '$bdate' , '$gender', '$phno' , '$email ', '$degree', '$school_grad' , '$year_grad' , '$award' , '$take' ,'$majorship', '$pay' ,'$image','$fb_name', '$password', 'pending')";
      mysqli_query($conn, $register);
     
      move_uploaded_file($_FILES["image"]["tmp_name"], "payment/".$_FILES["image"]["name"]);

      echo "<script>
         
              Swal.fire({
                   icon: 'success',
                   title: 'Submited Successful',
                   text: 'Check your email to know if your account has been approved!',
                   showConfirmButton: true,
                   confirmButtonText:'Proceed ',
                   confirmButtonColor:'#1E90FF ',
                   closeOnConfirm: false

                   })
                   .then(function() {
                    window.location = 'home.php';
                });

               </script>";
     
  }
}
?>



 </div>
  </div>
</div>



<script src="login-ui/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="login-ui/vendor/animsition/js/animsition.min.js"></script>
<script src="login-ui/vendor/bootstrap/js/popper.js"></script>
<script src="login-ui/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="login-ui/vendor/select2/select2.min.js"></script>
<script src="login-ui/vendor/daterangepicker/moment.min.js"></script>
<script src="login-ui/vendor/daterangepicker/daterangepicker.js"></script>
<script src="login-ui/vendor/countdowntime/countdowntime.js"></script>
<script src="login-ui/js/main.js"></script>





</body>
</html>



  <?php include 'conn.php';?>
<!DOCTYPE html>
<html lang="en">

<style>
    html {
    overflow: scroll;
    overflow-x: hidden;
    }
    ::-webkit-scrollbar {
    width: 0px; 
    ::-webkit-scrollbar-thumb {
    background: #FF0000;
    }
   </style>
<head>
	<title>CTE- Reviewer Center</title>
	<meta charset="UTF-8">
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="login-ui/image/png" href="login-ui/images/logo1.png"/>
	
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="login-ui/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/util.css">
	<link rel="stylesheet" type="text/css" href="login-ui/css/main.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<script src="js/script.js" defer></script>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url(login-ui/images/back.jpg);  background-position: center; background-size: cover; display: flex; ">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(login-ui/images/index.png);">
					<span class="login100-form-title-1">
						Enrollment 
					</span>
				</div>


   <!--enrollment starts-->
   <form class="form needs-validation"  method="POST"  enctype="multipart/form-data" novalidate>

      
      <!-- Progress bar -->
      <div class="progressbar">
        <div class="progress" id="progress"></div>
        
        <div
          class="progress-step progress-step-active"
          data-title="Personal "
        ></div>
        <div class="progress-step" data-title="Contact"></div>
        <div class="progress-step" data-title="School"></div>
        <div class="progress-step" data-title="Account"></div>
      </div>

      <!-- Steps -->
      <div class="form-step form-step-active">
      <center><label for=""><p><FONT size="5">Personal Information</p></FONT></label></center><br>
     
        <div class="form-group">
         <label for="validationCustom01"><p><font size="3">Full Name</font></p> </label> 
          <input type="text" name="username" id="validationCustom01" class="form-control" required/>
          <div class="invalid-feedback">
          Please input fullname required
          </div>
        </div>
        <div class="form-group">
         <label for="validationCustom02"> <p><font size="3">Middle Name</font></p></label>
          <input type="text"  class="form-control" name="mname" id="validationCustom02"   required/>
          <div class="invalid-feedback">
          Please input middle name required
          </div>
        </div>
        
        
      
        <div class="form-group ">
       <label for="validationCustom04"><p><font size="3">Gender</font></p></label> 
    <select class="form-control" name="gender" id="validationCustom04" required> 
   
    <option  value="" selected>Choose...</option>
      <option value="M">Male</option>
      <option value="F">Female</option>  
            </select>
            <div class="invalid-feedback">
            Please input gender required
          </div>
  </div>
 <br>
        <div class="btns-group">
          <a href="index.php" class="btn btn-prev ">Back</a>
          <a href="#" class="btn btn-next ">Next</a>
        </div>
      </div>
      
      <div class="form-step">
      <center><p><FONT size="5">Contact Information</p></FONT></center><br>
        <div class="form-group">
        <label for="validationCustom05"><p><font size="3">Contact Number (Globe or TM Only)</font></p></label>
          <input type="text" class="form-control" name="phno" id="validationCustom05" required/>
          <div class="invalid-feedback">
          Please input phone number required
          </div>
        </div>
        <div class="form-group ">
         <label for="validationCustom03"> <p><font size="3">Home Address</font></p></label>
          <input type="text"  class="form-control" name="address" id="validationCustom03"   required/>
          <div class="invalid-feedback">
          Please input home address required
          </div>
          </div>
       <br>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      
      
      <div class="form-step">
      <center><p><FONT size="5">School Information</p></FONT></center><br>
      
    
  <div class="form-group ">
  <label for="validationCustom09"><p><font size="3">School Graduate</font></p></label>
    <input type="text" class="form-control" id="validationCustom09" name="school_grad" required>
    <div class="invalid-feedback">
    Please input school graduate required
          </div>
  </div>
   
   
  <div class="form-group ">
  <label for=""><p><font size="3">Year Graduate</font></p></label>
    <input type="text" class="form-control" id="year_grad" name="year_grad" required >
    <div class="invalid-feedback">
    Please input year graduate required
          </div>
  </div>
  <div class="form-group ">
  <label for=""><p><font size="3">Profession</font></p></label>
    <input type="text" class="form-control" id="profession" name="profession" required >
    <div class="invalid-feedback">
    Please input profession required
          </div>
  </div>
  <div class="form-group ">
 <label for="validationCustom10"> <p><font size="3">Academic Award</font></p></label>
    <select class="form-control" name="award" id="validationCustom10" required>
    
    <option  value="" selected>Choose...</option>
      <option value="Cumlaude">Cumlaude</option>
      <option value="Magna Cumlaude">Magna Cumlaude</option>
      <option value="Summa Cumlaude">Summa Cumlaude</option>
	    <option value="None">None</option>
    </select>
    <div class="invalid-feedback">
    Please input Academic award required
          </div>
  </div>
  
  <br>
        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <a href="#" class="btn btn-next">Next</a>
        </div>
      </div>
      
      
      <div class="form-step">
      <center><p><FONT size="5">Account Information</p></FONT></center><br>
       <div class="form-group">
      <label for="validationCustom06"> <p><font size="3">Email</font></p></label> 
          <input type="text"  class="form-control" name="email" id="validationCustom06"  required/>
          <div class="invalid-feedback">
          Please input email required
          </div>
        </div>
        <div class="form-group">
        <label for="validationCustom16"><p><font size="3">Password</font></p></label>
          <input class="form-control" type="password" name="password" id="validationCustom16" required />
          <div class="invalid-feedback">
          Please input password required
          </div>
</div>
<div class="form-group">
<label for="validationCustom17"><p><font size="3">Confirm Password</font></p></label>
          <input class="form-control" type="password" name="Cpassword" id="validationCustom17" required />
          <div class="invalid-feedback">
            Please input confirm password required
          </div>
</div>
<div class="form-group">
<p><font size="3"> <b> Disclaimer</b></font></p>

<p><font size="3">All information entered in this site (including names ,email and address) sill be used
  exclusively for the stated purposes of this site and will not be made availabled for any other  purposes
  on to any other party. By checking the box below, you agree to be bound with the provisions of the 
  <b>Data Privacy Act 2012</b>
</font></p>

<div class="form-check">
<input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
<label class="form-check-label" for="invalidCheck"><p>Agree to terms and conditons</p></label>
<div class="invalid-feedback">
            Please check the box
          </div>

</div>
</div>
<br>

        <div class="btns-group">
          <a href="#" class="btn btn-prev">Previous</a>
          <input type="submit" value="Submit"  name ="submit" id="submit" class="btn" />
        </div>
      </div>
      
    </form>
<!--enrollment end-->

<script>
(function(){
  'use strict'
  var forms = document.querySelectorAll('.needs-validation')

  Array.prototype.slice.call(forms)
  .forEach(function(form){

    form.addEventListener('submit', function (event){
      if (!form.checkValidity()){
        event.preventDefault()
        event.stopPropagation()
      }
form.classList.add('was-validated')
    },false)
  })

})()

</script>

<?php
include 'connection.php';

if(isset($_POST['submit'])){


  $username      = $_POST['username'];
  $mname         = $_POST['mname'];
  $gender        = $_POST['gender'];
  $address       = $_POST['address'];
  $phno          = $_POST['phno'];
  $email         = $_POST['email'];
  $profession    = $_POST['profession'];
  $school_grad   = $_POST['school_grad'];
  $year_grad     = $_POST['year_grad'];
  $award         = $_POST['award'];
  $password      = $_POST['password'];
 

    $select = "SELECT * FROM admin_acc WHERE admin_user = '$email'";
    $result = mysqli_query($conn, $select);

    if(mysqli_num_rows($result) > 0){
      echo "<script>
           
      Swal.fire({
           icon: 'error',
           title: 'Ooops...',
           text: 'Email is already exist. Try again!',
           showConfirmButton: true,
           confirmButtonText:'Okay ',
           confirmButtonColor:'#1E90FF ',
           closeOnConfirm: false

           })

       </script>";
    }else{
        $register = "INSERT INTO admin_acc (admin_fullname, admin_mname ,  admin_gender, admin_contact, admin_user,admin_schlgrad, admin_yrgrad, admin_acad, admin_profession, admin_address, admin_pass) VALUES ('$username', '$mname'  , '$gender', '$phno' , '$email', '$school_grad' , '$year_grad' , '$award'  ,'$profession' ,'$address' , '$password')";
        mysqli_query($conn, $register);
       

        echo "<script>
           
                Swal.fire({
                     icon: 'success',
                     title: 'Submited Successful',
                     text: ' Thank You!',
                     showConfirmButton: true,
                     confirmButtonText:'Proceed ',
                     confirmButtonColor:'#1E90FF ',
                     closeOnConfirm: false

                     })
                     .then(function() {
                      window.location = 'home.php';
                  });

                 </script>";
       
    }
}
?>



   </div>
		</div>
	</div>
	


	<script src="login-ui/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="login-ui/vendor/animsition/js/animsition.min.js"></script>
	<script src="login-ui/vendor/bootstrap/js/popper.js"></script>
	<script src="login-ui/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="login-ui/vendor/select2/select2.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/moment.min.js"></script>
	<script src="login-ui/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="login-ui/vendor/countdowntime/countdowntime.js"></script>
	<script src="login-ui/js/main.js"></script>





</body>
</html>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



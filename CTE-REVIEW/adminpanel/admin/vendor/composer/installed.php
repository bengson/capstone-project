<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'ef71f2d20de78aad8dfcbf7ee9eff870667b7c04',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'ef71f2d20de78aad8dfcbf7ee9eff870667b7c04',
            'dev_requirement' => false,
        ),
        'php-ai/php-ml' => array(
            'pretty_version' => '0.9.0',
            'version' => '0.9.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-ai/php-ml',
            'aliases' => array(),
            'reference' => 'fd8b70629e16ed67307fe150ba25738449b9a6b2',
            'dev_requirement' => false,
        ),
    ),
);
